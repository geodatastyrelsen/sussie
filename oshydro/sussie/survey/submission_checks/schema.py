import json
import logging
import os

logger = logging.getLogger(__name__)


class Schema:

    def __init__(self, json_path: str) -> None:
        if not os.path.exists(json_path):
            raise RuntimeError("unable to locate %s" % json_path)
        self._json_path = json_path

        self._json = None
        with open(self._json_path, encoding='utf8') as f:
            self._json = json.load(f)

    @property
    def json_path(self) -> str:
        return self._json_path

    @property
    def json(self) -> dict:
        return self._json

    @property
    def title(self) -> str:
        return self.json["title"]

    @property
    def version(self) -> str:
        return self.json["version"]

    @property
    def structure(self) -> dict:
        return self.json["structure"]

    def __repr__(self) -> str:
        msg = "<%s>\n" % self.__class__.__name__

        msg += "  <json path: %s>\n" % self.json_path
        msg += "  <title: %s>\n" % self.title
        msg += "  <version: %s>\n" % self.version

        return msg
