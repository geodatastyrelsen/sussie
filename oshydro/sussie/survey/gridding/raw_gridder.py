import logging
import os

import numpy as np
from oshydro.base.formats.raw.raw import Raw
from oshydro.sussie.common.helper import Helper
from oshydro.sussie.common.progress.abstract_progress import AbstractProgress
from oshydro.sussie.common.progress.cli_progress import CliProgress
from oshydro.sussie.survey.gridding.grid_tile import GridTile
from shapefile import Writer

logger = logging.getLogger(__name__)


class RawGridder:

    def __init__(self, input_paths: list[str], progress: 'AbstractProgress' = CliProgress()) -> None:
        for input_path in input_paths:
            if not os.path.exists(input_path):
                raise RuntimeError('Unable to locate %s' % input_path)
        self._input_paths = input_paths
        self._tiles = dict()
        self._grid_res = 0.0
        self._tile_side = 0.0
        self._tile_cells = 1000
        self._output_path = str()

        # progress bar
        if not isinstance(progress, AbstractProgress):
            raise RuntimeError("invalid progress object")
        self.progress = progress

    def make_grid(self, grid_res: float, output_path: str, skip_flagged: bool) -> None:
        self._tiles = dict()
        self._grid_res = grid_res
        self._output_path = output_path

        logger.info("nr. of input files: %s" % len(self._input_paths))
        logger.info("output path: %s" % self._output_path)
        logger.info("grid resolution: %.2f m" % self._grid_res)

        self._tile_side = self._tile_cells * self._grid_res
        logger.debug('tile side: %f m (%d * %f)' % (self._tile_side, self._tile_cells, self._grid_res))

        total_dg = 10000  # Cost of writing file
        for idx, input_path in enumerate(self._input_paths):
            raw = Raw(path=input_path)
            raw.check_variant()
            raw.read_header()
            raw.read_body()
            total_dg += raw.body.nr_of_datagrams

        self.progress.start(title='Gridding', text='Reading files', has_abortion=True, max_value=total_dg)

        for idx, input_path in enumerate(self._input_paths):
            if self.progress.canceled:
                break
            logger.info('%d/%d: reading from %s'
                        % (idx + 1, len(self._input_paths), input_path))

            self._make_grid_from_file(input_path=input_path, skip_flagged=skip_flagged)

        if self.progress.canceled:
            logger.info('Gridding ... CANCELLED!')
            self.progress.end()
            return

        logger.info('Writing output ...')
        output_ext = os.path.splitext(output_path)[-1]
        if output_ext == '.xyz':

            self.progress.add(quantum=5, text='Writing xyz')

            with open(self._output_path, 'w') as fod:

                logger.info('nr of tiles: %s' % len(self._tiles))
                for idx, key in enumerate(self._tiles.keys()):
                    logger.info("%d/%d: reading from '%s' tile"
                                % (idx + 1, len(self._tiles), key))

                    gt = self._tiles[key]
                    valid_z = np.argwhere(np.isfinite(gt.z))
                    logger.debug('valid points: %s' % (valid_z.shape[0]))

                    for pair in valid_z:
                        fod.write('%.3f, %.3f, %.3f\n'
                                  % (gt.x[pair[0], pair[1]], gt.y[pair[0], pair[1]], gt.z[pair[0], pair[1]]))
            self.progress.add(quantum=9995)

        elif output_ext == '.shp':
            self.progress.add(quantum=5, text='Writing shp')

            with Writer(self._output_path) as w:
                w.field('Depth', 'N')
                for idx, key in enumerate(self._tiles.keys()):
                    logger.info("%d/%d: reading from '%s' tile"
                                % (idx + 1, len(self._tiles), key))

                    gt = self._tiles[key]
                    valid_z = np.argwhere(np.isfinite(gt.z))
                    logger.debug('valid points: %s' % (valid_z.shape[0]))

                    for pair in valid_z:
                        w.pointz(gt.x[pair[0], pair[1]], gt.y[pair[0], pair[1]], gt.z[pair[0], pair[1]])
                        w.record(gt.z[pair[0], pair[1]])
            self.progress.add(quantum=9995)
        else:
            raise RuntimeError('Unsupported format extension: %s' % output_ext)

        logger.info('Writing output ... DONE!')
        self.progress.end()
        Helper.explore_folder(os.path.dirname(self._output_path))

    def _make_grid_from_file(self, input_path: str, skip_flagged: bool) -> None:
        raw = Raw(path=input_path)
        raw.check_variant()
        raw.read_header()
        raw.read_body()
        nr_of_dg = raw.body.nr_of_datagrams
        logger.debug("nr of datagrams: %s" % nr_of_dg)

        dg_pct_add = nr_of_dg // 10000  # Update every 10000 makes gui fairly responsive
        dg_pct = max(dg_pct_add, 1)
        dg_pct_added = 0
        self.progress.add(0, "Gridding %s" % input_path)

        for i, dgf_f in enumerate(raw.body.iter_datagrams()):
            if self.progress.canceled:
                return
            if (i + 1) % dg_pct == 0:
                self.progress.add(dg_pct_add)
                dg_pct_added += dg_pct_add

            # logger.debug("sounding @%d -> %s" % (i, dgf))
            dgf = dgf_f["fields"]
            if skip_flagged:
                dgf_quality = dgf['quality']
                if dgf_quality >= 127:
                    continue
            dgf_n = dgf['n'] / 100.0
            dgf_e = dgf['e'] / 100.0
            dgf_depth = dgf['depth'] / 100

            tile_x = dgf_e // self._tile_side
            tile_y = dgf_n // self._tile_side
            tile_key = '%d_%d' % (tile_x, tile_y)
            # logger.debug('tile key: %s (%s, %s <-> %s)' % (tile_key, dgf_e, dgf_n, self._tile_side))
            if tile_key not in self._tiles:
                self._tiles[tile_key] = GridTile()
            gt = self._tiles[tile_key]

            tile_i = int((dgf_n % self._tile_side) // self._grid_res)
            tile_j = int((dgf_e % self._tile_side) // self._grid_res)
            # logger.debug('%s, %s <-> %s, %s' % (dgf_e, dgf_n, tile_i, tile_j))
            tile_z = gt.z[tile_i, tile_j]

            if np.isnan(tile_z):
                gt.z[tile_i, tile_j] = dgf_depth
                gt.x[tile_i, tile_j] = dgf_e
                gt.y[tile_i, tile_j] = dgf_n
            elif dgf_depth < tile_z:
                gt.z[tile_i, tile_j] = dgf_depth
                gt.x[tile_i, tile_j] = dgf_e
                gt.y[tile_i, tile_j] = dgf_n

        self.progress.add(nr_of_dg - dg_pct_added)
