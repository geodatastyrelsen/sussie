import copy
import json
import logging
import os
import shutil
from datetime import datetime

from oshydro.base.formats.raw.raw import Raw
from oshydro.sussie import __version__
from oshydro.sussie.common.progress.abstract_progress import AbstractProgress
from oshydro.sussie.common.progress.cli_progress import CliProgress
from oshydro.sussie.survey.position_analyzer.duplicates_checker import DuplicatesChecker
from oshydro.sussie.survey.position_analyzer.outliers_checker import OutliersChecker

logger = logging.getLogger(__name__)


class PositionAnalyzer:

    def __init__(self, output_folder: str, progress=CliProgress()) -> None:
        # progress bar
        if not isinstance(progress, AbstractProgress):
            raise RuntimeError("invalid progress object")

        self._output_folder = output_folder
        self._progress = progress

        self._report: dict | None = None
        self._fp: dict | None = None
        self._report_path: str | None = None
        self._mini_report_path: str | None = None
        self.computation_time = 0.0

    def clear_data(self) -> None:

        self._report = None
        self._fp = None
        self._report_path = None
        self._mini_report_path = None
        self.computation_time = 0.0

    @property
    def report(self) -> dict | None:
        return self._report

    @property
    def mini_report(self) -> dict | None:
        return self._copy_dict(self._report, ["duplicate_points", "outlier_points"])

    @property
    def fp(self) -> dict | None:
        return self._fp

    @property
    def report_path(self) -> str | None:
        return self._report_path

    @property
    def mini_report_path(self) -> str | None:
        return self._mini_report_path

    @classmethod
    def is_valid_position(cls, dgf: dict) -> bool:
        return OutliersChecker.is_valid_position(dgf)

    @property
    def errors(self) -> int:
        errors = self.outliers_flagged or 0
        errors += self.outliers_unflagged or 0
        errors += self.duplicates_flagged or 0
        errors += self.duplicates_unflagged or 0
        return errors

    @property
    def error_files(self) -> int | None:
        return self._report_keys_count(["nr_of_flagged_duplicates", "nr_of_unflagged_duplicates",
                                        "nr_of_flagged_outliers", "nr_of_unflagged_outliers"])

    @property
    def duplicates_flagged(self) -> int | None:
        return self._report_key_sum("nr_of_flagged_duplicates")

    @property
    def duplicates_unflagged(self) -> int | None:
        return self._report_key_sum("nr_of_unflagged_duplicates")

    @property
    def duplicates_files(self) -> int | None:
        return self._report_keys_count(["nr_of_flagged_duplicates", "nr_of_unflagged_duplicates"])

    @property
    def outliers_flagged(self) -> int | None:
        return self._report_key_sum("nr_of_flagged_outliers")

    @property
    def outliers_unflagged(self) -> int | None:
        return self._report_key_sum("nr_of_unflagged_outliers")

    @property
    def outliers_files(self) -> int | None:
        return self._report_keys_count(["nr_of_flagged_outliers", "nr_of_unflagged_outliers"])

    def _report_key_sum(self, key: str) -> int | None:
        val = None
        if self._report and key in next(iter(self._report["checked"].values())):
            return sum([r[key] for r in self._report["checked"].values()])
        return val

    def _report_keys_count(self, keys: list[str]) -> int | None:
        val = None
        path_report = next(iter(self._report["checked"].values()))
        if set(path_report.keys()).intersection(set(keys)):
            val = 0
            for rep in self._report["checked"].values():
                for key in keys:
                    if rep.get(key, 0) > 0:
                        val += 1
                        break

        return val

    @classmethod
    def _copy_dict(cls, input_dict: dict, except_keys: list[str]) -> dict:
        # Deep copies dictionary except for except_keys keys.
        output_dict = {}
        for k, v in input_dict.items():
            if k in except_keys:
                continue
            if isinstance(v, dict):
                output_dict[k] = cls._copy_dict(v, except_keys)
            else:
                output_dict[k] = copy.deepcopy(v)
        return output_dict

    def _write_report(self, full_json: bool) -> None:
        self._report["bbox"] = dict()
        for path, single_report in self._report["checked"].items():
            if "bbox" in single_report:
                for key in ["min_n", "min_e", "max_n", "max_e"]:
                    bbox_key = single_report["bbox"][key]
                    if key not in self._report["bbox"]:
                        self._report["bbox"][key] = bbox_key
                    else:
                        if key in ["min_n", "min_e"]:
                            if bbox_key < self._report["bbox"][key]:
                                self._report["bbox"][key] = bbox_key
                        else:
                            if bbox_key > self._report["bbox"][key]:
                                self._report["bbox"][key] = bbox_key
        # write a minimal json (without all the outliers)
        with open(self._mini_report_path, "w") as fod:
            json.dump(self.mini_report, fod, indent=4)

        if full_json:
            # Write full json
            with open(self._report_path, "w") as fod:
                json.dump(self._report, fod, indent=4)

    def check_positions(self, paths: list[str], root_path: str | None, full_json: bool = False,
                        check_outliers: bool = False, check_duplicates: bool = False) -> None:

        start_time = datetime.now()
        timestamp = datetime.utcnow().strftime("%Y%m%d_%H%M%S")
        checks = []
        if check_outliers:
            checks.append("positional outliers")
        if check_duplicates:
            checks.append("duplicates")

        total_dg = 0
        for idx, input_path in enumerate(paths):
            raw = Raw(path=input_path)
            raw.check_variant()
            raw.read_header()
            raw.read_body()
            total_dg += raw.body.nr_of_datagrams

        self._progress.start(title="Check positions", text="Checking ...", has_abortion=True, max_value=total_dg)

        self._report = dict()
        self._report["version"] = __version__
        self._report["checked"] = dict()

        if root_path is not None:
            self._report["root_path"] = root_path

        self._report_path = os.path.join(self._output_folder, "checked.%s.json" % timestamp)
        self._mini_report_path = os.path.join(self._output_folder, "checked.minimal.%s.json" % timestamp)

        nr_paths = len(paths)

        logger.debug("checking %d files for %s " % (nr_paths, " and ".join(checks)))
        for idx, path in enumerate(paths):
            if self._progress.canceled:
                break
            self._progress.add(quantum=0.0, text="Checking ... %d/%d\n%s" % (idx + 1, nr_paths, path))
            beg_time = datetime.now()
            file_path = path
            if root_path is not None:
                file_path = os.path.relpath(path, root_path)

            file_report = self._check_file(path=path, full_json=full_json,
                                           check_outliers=check_outliers, check_duplicates=check_duplicates)
            if self._progress.canceled:
                break

            self._report["checked"][file_path] = file_report
            self._write_report(full_json=full_json)

            comp_time = (datetime.now() - beg_time).total_seconds() / 60.0
            logger.info("%d -> Checked %s -> time: %.2f mins" % (idx, path, comp_time))
            self._progress.add(quantum=0.0, text="Saving JSON ... %d/%d\n%s" % (idx + 1, nr_paths, path))

        if check_outliers:
            logger.info("Identified %d flagged / %d unflagged positional outliers in %d files." %
                        (self.outliers_flagged, self.outliers_unflagged, self.outliers_files))

        if check_duplicates:
            logger.info("Identified %d flagged / %d unflagged duplicates in %d files." %
                        (self.duplicates_flagged, self.duplicates_unflagged, self.duplicates_files))

        if self._progress.canceled:
            status = "CANCELLED"
        else:
            status = "DONE"

        self.computation_time = (datetime.now() - start_time).total_seconds() / 60.0
        logger.info("Check %s ... %s! -> time: %.2f mins" % (checks, status, self.computation_time))
        self._progress.end()

    def _check_file(self, path: str, check_outliers: bool, check_duplicates: bool, full_json: bool) -> dict:
        if not os.path.exists(path):
            raise RuntimeError("Unable to locate: %s" % path)

        raw = Raw(path=path)
        raw.check_variant()
        raw.read_header()
        raw.read_body()
        nr_of_dg = raw.body.nr_of_datagrams
        logger.debug("%s -> nr of datagrams: %s" % (path, nr_of_dg))

        dg_pct_add = nr_of_dg // 10000  # Update every 10000 makes gui fairly responsive
        dg_pct = max(dg_pct_add, 1)
        dg_pct_added = 0

        outlier_checker = None
        if check_outliers:
            outlier_checker = OutliersChecker(full_json=True)
            outlier_checker.init_check(raw)

        dup_checker = None
        if check_duplicates:
            dup_checker = DuplicatesChecker(full_json=full_json)

        for i, dgf_f in enumerate(raw.body.iter_datagrams()):
            if self._progress.canceled:
                return dict()
            # logger.debug("sounding @%d -> %s" % (i, dgf))
            if (i + 1) % dg_pct == 0:
                self._progress.add(dg_pct_add)
                dg_pct_added += dg_pct_add
            if check_outliers:
                outlier_checker.is_outlier(dgf_f, i)
            if check_duplicates:
                dup_checker.is_duplicate(dgf_f, i)

        report = dict()
        if check_outliers:
            report.update(outlier_checker.report())
        if check_duplicates:
            report.update(dup_checker.report())

        self._progress.add(nr_of_dg - dg_pct_added)

        return report

    def fix_positions(self, json_path: str, paths: list[str] | None, root_path: str | None = None,
                      remove_outliers: bool = False, remove_duplicates: bool = False) -> None:
        start_time = datetime.now()

        if ".minimal." in json_path:
            raise RuntimeError("Cannot fix positions from minimal JSON: %s" % json_path)

        self._report_path = os.path.join(self._output_folder, "fixed.%s.json"
                                         % (datetime.utcnow().strftime("%Y%m%d_%H%M%S")))

        with open(json_path) as fid:
            self._report = json.load(fid)

        if paths is None:
            paths = list(self._report["checked"].keys())

        if root_path is None:
            root_path = self._report["root_path"]
        if not os.path.exists(root_path):
            raise RuntimeError("unable to locate root path: %s" % root_path)

        self._fp = dict()
        self._fp["version"] = __version__
        self._fp["fixed"] = dict()

        if remove_outliers:
            logger.debug("fixing strategy: delete invalid points")
            self._fp["fixed"]["fixing_strategy"] = "delete invalid points"

        if root_path is not None:
            self._fp["root_path"] = root_path

        fix_paths = list()
        fix_count = 0
        logger.info("Files to be fixed:")
        nr_of_issues = 0
        for path in paths:
            issue_dgs = (len(self._report["checked"][path].get("outlier_points", [])) +
                         len(self._report["checked"][path].get("duplicate_points", [])))
            if issue_dgs > 0:
                nr_of_issues += issue_dgs
                fix_paths.append(path)
                fix_count += 1
                logger.info("%03d: %s" % (fix_count, path))

        self._progress.start(title="Fix positions", text="Fixing ...", max_value=nr_of_issues, has_abortion=True)

        if len(fix_paths) == 0:
            logger.info("No files to fix")
            self._progress.end()
            return

        nr_paths = len(fix_paths)

        logger.debug("Fixing %d files for invalid points" % nr_paths)
        for idx, path in enumerate(fix_paths):
            if self._progress.canceled:
                break
            self._progress.add(quantum=0.0, text="Fixing ... %d/%d\n%s" % (idx + 1, nr_paths, path))
            beg_time = datetime.now()

            file_report = self._fau_fu2_fix_positions(path=path, root_path=root_path, remove_outliers=remove_outliers,
                                                      remove_duplicates=remove_duplicates)
            if self._progress.canceled:
                break
            self._fp["fixed"][path] = file_report

            comp_time = (datetime.now() - beg_time).total_seconds() / 60.0
            logger.info("%d -> Checked %s -> time: %.2f mins" % (idx, path, comp_time))
            self._progress.add(quantum=0.0, text="Saving JSON ... %d/%d\n%s" % (idx + 1, nr_paths, path))
            with open(self._report_path, "w") as fod:
                json.dump(self._fp, fod, indent=4)

        if self._progress.canceled:
            status = "CANCELLED"
        else:
            status = "DONE"

        self.computation_time = (datetime.now() - start_time).total_seconds() / 60.0
        logger.info("Fix invalid points ... %s! -> time: %.2f mins" % (status, self.computation_time))
        self._progress.end()

    def _fau_fu2_fix_positions(self, path: str, root_path: str | None,
                               remove_outliers: bool = False, remove_duplicates: bool = False) -> dict:
        if root_path is not None:
            full_path = os.path.join(root_path, path)
        else:
            full_path = path

        if not os.path.exists(full_path):
            raise RuntimeError("Unable to locate: %s" % full_path)

        bk_path = "%s.bk" % full_path
        if os.path.exists(bk_path):
            raise RuntimeError("An existing .bk exists, first rename it (e.g., .bk2) -> %s" % bk_path)

        logger.debug("copying %s -> %s ..." % (os.path.basename(full_path), os.path.basename(bk_path)))
        shutil.move(full_path, bk_path)
        logger.debug("copying %s -> %s ... DONE" % (os.path.basename(full_path), os.path.basename(bk_path)))

        report = dict()
        report["fixed_points"] = dict()

        raw = Raw(path=bk_path)
        raw.check_variant()
        raw.read_header()
        raw.read_body()
        nr_of_dg = raw.body.nr_of_datagrams
        logger.debug("nr of datagrams: %s" % nr_of_dg)

        outlier_dgs = set()
        nr_of_issues = 0
        if remove_outliers:
            outlier_dgs = set([int(key) for key in self._report["checked"][path]["outlier_points"]])
            nr_of_issues += len(outlier_dgs)
            # logger.debug("issue datagrams: %s -> %s" % (nr_of_issues, issue_dgs))
            logger.debug("Issue datagrams: %s" % nr_of_issues)

        duplicate_dgs = set()
        if remove_duplicates:
            duplicate_dgs = set([int(key) for key in self._report["checked"][path].get("duplicate_points", set())])
            nr_of_duplicates = len(duplicate_dgs)
            nr_of_issues += nr_of_duplicates
            # logger.debug("duplicate datagrams: %s -> %s" % (nr_of_duplicates, duplicate_dgs))
            logger.debug("Duplicate datagrams: %s" % nr_of_duplicates)

        if raw.header:
            raw.header.nr_of_pings = 0
            raw.header.nr_of_beams = 0
            raw.header.write(output_path=full_path)

        start_dgs = None
        counter = 0
        counter_flagged = 0
        counter_unflagged = 0

        dg_pct_add = nr_of_issues // 10000  # Update every 10000 makes gui fairly responsive
        dg_pct = max(dg_pct_add, 1)
        dg_pct_added = 0

        for idx in range(raw.body.nr_of_datagrams):
            if self._progress.canceled:
                return dict()
            is_outlier_dg = idx in outlier_dgs
            is_duplicate_dg = idx in duplicate_dgs
            if is_outlier_dg or is_duplicate_dg:
                if (counter + 1) % dg_pct == 0:
                    self._progress.add(dg_pct_add)
                    dg_pct_added += dg_pct_add
                if start_dgs is not None:
                    # Fetch and write all non-issue datagrams since start_idx till idx - 1
                    buffer = raw.body.get_raw_datagrams(start_dgs, idx - 1)
                    logger.debug("buffer size: %d" % len(buffer))
                    raw.body.write_raw_datagram(buffer=buffer, output_path=full_path, append=True, keep_open=True)
                    start_dgs = None
                if is_outlier_dg:
                    dg = raw.body.get_datagram(idx)
                    # count nr of flagged/unflagged before fixing
                    dg_quality = dg["fields"]["quality"]
                    if dg_quality > 127:
                        counter_flagged += 1
                    else:
                        counter_unflagged += 1

                # logger.debug(" - %d/%d datagram -> deleted" % (idx, nr_of_dg))
                if idx in outlier_dgs:
                    report["fixed_points"][idx] = self._report["checked"][path]["outlier_points"]["%d" % idx]
                if idx in duplicate_dgs:
                    report["fixed_points"][idx] = self._report["checked"][path]["duplicate_points"]["%d" % idx]
                counter += 1

            else:
                if start_dgs is None:
                    start_dgs = idx

        if start_dgs is not None:
            buffer = raw.body.get_raw_datagrams(start_dgs, raw.body.nr_of_datagrams - 1)
            logger.debug("final buffer size: %d" % len(buffer))
            raw.body.write_raw_datagram(buffer=buffer, output_path=full_path, append=True, keep_open=True)

        raw.body.close_source()
        raw.body.ends_writing()
        if remove_outliers:
            if counter == counter_flagged:
                logger.info("fixed %d flagged points" % counter)
            elif counter == counter_unflagged:
                logger.info("fixed %d unflagged points" % counter)
            else:
                logger.info(
                    "fixed %d points: %d flagged and %d unflagged" % (counter, counter_flagged, counter_unflagged))
        self._progress.add(nr_of_issues - dg_pct_added)
        return report
