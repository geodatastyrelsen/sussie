import logging

logger = logging.getLogger(__name__)


class DuplicatesChecker:
    def __init__(self, full_json: bool) -> None:
        self._full_json = full_json

        self._duplicates_flagged = 0
        self._duplicates_unflagged = 0
        self._duplicate_points = dict()

        self._dupchecker = set()

    def is_duplicate(self, dgf_f: dict, i: int) -> None:
        dgf = dgf_f["fields"]
        lookup = hash((dgf["n"], dgf["e"], dgf["depth"], dgf["quality"]))
        if lookup in self._dupchecker:
            if self._full_json:
                cur_pos = dict()
                cur_pos["n"] = dgf["n"]
                cur_pos["e"] = dgf["e"]
                cur_pos["depth"] = dgf["depth"]
                cur_pos["quality"] = dgf["quality"]
                self._duplicate_points[i] = cur_pos
            if dgf["quality"] > 127:
                self._duplicates_flagged += 1
            else:
                self._duplicates_unflagged += 1
        else:
            self._dupchecker.add(lookup)

    def report(self) -> dict:
        report = dict()
        report["nr_of_flagged_duplicates"] = self._duplicates_flagged
        report["nr_of_unflagged_duplicates"] = self._duplicates_unflagged
        report["duplicate_points"] = self._duplicate_points
        return report
