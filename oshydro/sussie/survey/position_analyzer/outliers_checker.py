import logging

from oshydro.base.formats.raw.raw import Raw

logger = logging.getLogger(__name__)


class OutliersChecker:
    def __init__(self, full_json: bool) -> None:
        self._full_json = full_json

        self._bbox_min_n: int | None = None
        self._bbox_min_e: int | None = None
        self._bbox_max_n: int | None = None
        self._bbox_max_e: int | None = None

        self._outliers_flagged = 0
        self._outliers_unflagged = 0
        self._info_outlier_checker: str | None = None
        self._outlier_points = dict()

        self._cur_n: int | None = None
        self._cur_e: int | None = None

    def init_check(self, raw: Raw) -> bool:

        # initialize the current position
        for i, dgf_f in enumerate(raw.body.iter_datagrams()):
            dgf = dgf_f["fields"]
            logger.debug("sounding @%d -> %s" % (i, dgf))

            if self.is_valid_position(dgf=dgf):
                self._cur_n = dgf["n"]
                self._cur_e = dgf["e"]
                self._bbox_min_n = dgf["n"]
                self._bbox_min_e = dgf["e"]
                self._bbox_max_n = dgf["n"]
                self._bbox_max_e = dgf["e"]
                return True
        msg = "All soundings have an invalid position."
        self._info_outlier_checker = msg
        logger.warning(msg)
        return False

    @classmethod
    def is_valid_position(cls, dgf: dict) -> bool:
        if dgf["n"] < 576151000:  # 52 deg N
            return False
        if dgf["n"] > 932900500:  # 84 deg N
            return False
        if dgf["e"] > 150000000:
            return False
        if dgf["e"] < -50000000:
            return False

        return True

    def is_outlier(self, dgf_f: dict, i: int) -> None:
        dgf = dgf_f["fields"]
        dgf_n = dgf["n"]
        dgf_e = dgf["e"]
        dgf_depth = dgf["depth"]
        dgf_quality = dgf["quality"]
        is_outlier = False
        if not self.is_valid_position(dgf=dgf):
            is_outlier = True
            # logger.warning("#%d -> invalid point @%s -> (%s, %s)" % (i, dgf_n, dgf_e))
        else:
            dn = (self._cur_n - dgf_n) / 100000.0  # converting to km
            de = (self._cur_e - dgf_e) / 100000.0  # concerting to km
            dist_2 = (dn * dn + de * de)  # we keep the squared value for performance reasons
            if dist_2 > 250000.0:  # = 500 km ^2
                # logger.warning("#%d -> too distant point @%s -> (%s, %s) vs. (%s, %s) -> %s"
                #               % (i, dgf["n"], dgf["e"], self._cur_n, self._cur_e, dist_2))
                is_outlier = True
        if is_outlier:
            outlier = dict()
            outlier["n"] = dgf_n
            outlier["e"] = dgf_e
            outlier["depth"] = dgf_depth
            outlier["quality"] = dgf_quality
            self._outlier_points[i] = outlier
            if dgf_quality > 127:
                self._outliers_flagged += 1
            else:
                self._outliers_unflagged += 1
        else:
            self._cur_n = dgf_n
            self._cur_e = dgf_e

            if dgf_n < self._bbox_min_n:
                self._bbox_min_n = dgf_n
            elif dgf_n > self._bbox_max_n:
                self._bbox_max_n = dgf_n

            if dgf_e < self._bbox_min_e:
                self._bbox_min_e = dgf_e
            elif dgf_e > self._bbox_max_e:
                self._bbox_max_e = dgf_e

    def report(self) -> dict:
        report = dict()
        if self._info_outlier_checker is not None:
            report["info_outlier_checker"] = self._info_outlier_checker
        report["bbox"] = dict()
        report["bbox"]["min_n"] = self._bbox_min_n
        report["bbox"]["min_e"] = self._bbox_min_e
        report["bbox"]["max_n"] = self._bbox_max_n
        report["bbox"]["max_e"] = self._bbox_max_e

        report["nr_of_flagged_outliers"] = self._outliers_flagged
        report["nr_of_unflagged_outliers"] = self._outliers_unflagged
        report["outlier_points"] = self._outlier_points
        return report
