"""
Hydro-Package
Support Utility for Survey Information and Enhancement
"""
import logging
import os

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

name = 'Sussie'
__version__ = '1.12.1'
__author__ = ('Giuseppe Masetti; Lasse M. Schwenger; Jonas Madsen; Kristian V. Kristmar; Nicki R. Andreasen; '
              'Ove Andersen; Philip S. Christiansen; Sotirios Skarpalezos; James P. Harris')
author_email = ('gimas@gst.dk; lamon@gst.dk; jomad@gst.dk; krkri@gst.dk; nirib@gst.dk; ovand@gst.dk; phsic@gst.dk; '
                'soska@gst.dk; japeh@gst.dk')
__license__ = 'MIT license'
__copyright__ = 'Copyright 2025 Sussie Dev Team'
media_path = os.path.join(os.path.dirname(__file__), 'app', 'media')
if not os.path.exists(media_path):
    raise RuntimeError('Missing media folder at %s' % media_path)
favicon_path = os.path.join(media_path, 'favicon.png')
if not os.path.exists(favicon_path):
    raise RuntimeError('Missing favicon at %s' % favicon_path)
license_path = os.path.join(media_path, 'LICENSE')
if not os.path.exists(license_path):
    raise RuntimeError('Missing LICENSE at %s' % favicon_path)
