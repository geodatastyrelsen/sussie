import logging
import os

from PySide6 import QtWidgets, QtGui, QtCore

from oshydro.sussie.common.helper import Helper
from oshydro.sussie.survey.project import Project

logger = logging.getLogger(__name__)


class Xyz2Fau(QtWidgets.QMainWindow):
    media = os.path.join(os.path.dirname(__file__), 'media')

    def __init__(self, parent: QtWidgets.QMainWindow | None = None, title: str | None = "FAU Converter",
                 folder_processing: bool = False) -> None:
        QtWidgets.QMainWindow.__init__(self, parent)

        if isinstance(title, str):
            self.setWindowTitle(title)

        self._folder_processing = folder_processing

        if parent is None:
            icon_info = QtCore.QFileInfo(os.path.join(self.media, 'favicon.png'))
            self.setWindowIcon(QtGui.QIcon(icon_info.absoluteFilePath()))
            if Helper.is_windows():

                try:
                    # This is needed to display the app icon on the taskbar on Windows 7
                    import ctypes
                    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(title)

                except AttributeError as e:
                    logger.debug("Unable to change app icon: %s" % e)

        self.setMinimumSize(600, 450)

        frame = QtWidgets.QFrame()
        self.setCentralWidget(frame)

        self.vbox = QtWidgets.QVBoxLayout()
        frame.setLayout(self.vbox)

        lbl_width = 130
        wdg_width = 80

        # input filename
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_files = QtWidgets.QLabel("Input path:")

        hbox.addWidget(text_files)
        text_files.setFixedWidth(lbl_width)
        self.input_path = QtWidgets.QListWidget()
        self.input_path.setFixedHeight(24)
        hbox.addWidget(self.input_path)
        self.input_path.setAlternatingRowColors(True)
        input_button = QtWidgets.QPushButton("..")
        input_button.setFixedWidth(30)
        hbox.addWidget(input_button)
        input_button.clicked.connect(self.on_set_input_path)

        # output filename
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_output = QtWidgets.QLabel("Output path:")
        hbox.addWidget(text_output)
        text_output.setFixedWidth(lbl_width)
        self.output_path = QtWidgets.QListWidget()
        self.output_path.setFixedHeight(24)
        hbox.addWidget(self.output_path)
        self.output_path.setAlternatingRowColors(True)
        output_button = QtWidgets.QPushButton("..")
        output_button.setFixedWidth(30)
        hbox.addWidget(output_button)
        output_button.clicked.connect(self.on_set_output_path)

        if self._folder_processing:
            self.setMinimumSize(600, 500)
            # Scan folders recursive
            hbox = QtWidgets.QHBoxLayout()
            self.vbox.addLayout(hbox)
            text_skip = QtWidgets.QLabel("Scan subfolders:")
            hbox.addWidget(text_skip)
            text_skip.setFixedWidth(lbl_width)
            self.set_recursive = QtWidgets.QCheckBox()
            self.set_recursive.setChecked(False)
            hbox.addWidget(self.set_recursive)
            hbox.addStretch()

            # Overwrite existing files
            hbox = QtWidgets.QHBoxLayout()
            self.vbox.addLayout(hbox)
            text_skip = QtWidgets.QLabel("Overwrite files:")
            hbox.addWidget(text_skip)
            text_skip.setFixedWidth(lbl_width)
            self.set_overwrite = QtWidgets.QCheckBox()
            self.set_overwrite.setChecked(False)
            hbox.addWidget(self.set_overwrite)
            hbox.addStretch()
            print("Overwrite?", self.set_recursive.isChecked())

        self.vbox.addSpacing(12)

        # geographic
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_geographic = QtWidgets.QLabel("Geographic coordinates:")
        hbox.addWidget(text_geographic)
        text_geographic.setFixedWidth(lbl_width)
        self.set_geographic = QtWidgets.QCheckBox()
        hbox.addWidget(self.set_geographic)

        # geographic - input crs
        self.input_epsg_code = None
        self.input_crs_dict = dict()
        self._populate_input_crs()

        self.input_crs_combo = QtWidgets.QComboBox()
        self.input_crs_combo.setFixedWidth(450)
        self.input_crs_combo.setVisible(False)
        self.input_crs_combo.addItems(self.input_crs_dict)
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        self.text_input_crs = QtWidgets.QLabel("Input coordinate system:")
        self.text_input_crs.setVisible(False)

        hbox.addWidget(self.text_input_crs)
        self.text_input_crs.setFixedWidth(lbl_width)
        hbox.addWidget(self.input_crs_combo)
        hbox.addStretch()

        # geographic - output crs
        self.output_epsg_code = None
        self.output_crs_dict = dict()
        self._populate_output_crs()

        self.output_crs_combo = QtWidgets.QComboBox()
        self.output_crs_combo.setFixedWidth(450)
        self.output_crs_combo.setVisible(False)
        self.output_crs_combo.addItems(self.output_crs_dict)
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        self.text_output_crs = QtWidgets.QLabel("Output coordinate system:")
        self.text_output_crs.setVisible(False)

        hbox.addWidget(self.text_output_crs)
        self.text_output_crs.setFixedWidth(lbl_width)
        hbox.addWidget(self.output_crs_combo)
        hbox.addStretch()
        self.set_geographic.stateChanged.connect(self._toggle_crs_selection)

        # time-tagged
        self.timetag_index = None
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_timetag = QtWidgets.QLabel("Time-tagged:")
        hbox.addWidget(text_timetag)
        text_timetag.setFixedWidth(lbl_width)
        self.set_timeflag = QtWidgets.QCheckBox()
        hbox.addWidget(self.set_timeflag)
        hbox.addStretch()
        self.set_timeflag.stateChanged.connect(self._toggle_time_tag)

        # checkbox no-data value
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_ndv = QtWidgets.QLabel("Set No-data value:")
        hbox.addWidget(text_ndv)
        text_ndv.setFixedWidth(lbl_width)
        self.set_ndv_cb = QtWidgets.QCheckBox()
        hbox.addWidget(self.set_ndv_cb)

        # no-data value
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        self.set_ndv = QtWidgets.QDoubleSpinBox()
        self.set_ndv.setFixedWidth(wdg_width)
        self.set_ndv.setDecimals(3)
        self.set_ndv.setValue(0.0)
        self.set_ndv.setVisible(False)
        hbox.addWidget(self.set_ndv)
        hbox.addStretch()
        self.set_ndv_cb.stateChanged.connect(self._toggle_ndv)
        # skip row
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_skip = QtWidgets.QLabel("Skip rows:")
        hbox.addWidget(text_skip)
        text_skip.setFixedWidth(lbl_width)
        self.set_skip = QtWidgets.QSpinBox()
        self.set_skip.setFixedWidth(wdg_width)
        self.set_skip.setMaximum(9999)
        self.set_skip.setValue(1)
        hbox.addWidget(self.set_skip)
        hbox.addStretch()

        # separator
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_separator = QtWidgets.QLabel("Column separator:")
        hbox.addWidget(text_separator)
        text_separator.setFixedWidth(lbl_width)
        self.set_separator = QtWidgets.QComboBox()
        self.set_separator.setFixedWidth(wdg_width)
        self.set_separator.addItems([',', ';', 'TAB', 'SPACE'])
        hbox.addWidget(self.set_separator)
        hbox.addStretch()

        # set xyz columns
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_columns = QtWidgets.QLabel("Column Indices:")
        hbox.addWidget(text_columns)
        text_columns.setFixedWidth(lbl_width)
        # x
        text_easting = QtWidgets.QLabel("x")
        hbox.addWidget(text_easting)
        text_easting.setFixedWidth(5)
        self.set_easting = QtWidgets.QSpinBox()
        self.set_easting.setFixedWidth(70)
        self.set_easting.setMaximum(9999)
        self.set_easting.setValue(0)
        hbox.addWidget(self.set_easting)
        hbox.addSpacing(20)
        # y
        text_northing = QtWidgets.QLabel("y")
        hbox.addWidget(text_northing)
        text_northing.setFixedWidth(5)
        self.set_northing = QtWidgets.QSpinBox()
        self.set_northing.setFixedWidth(70)
        self.set_northing.setMaximum(9999)
        self.set_northing.setValue(1)
        hbox.addWidget(self.set_northing)
        hbox.addSpacing(20)
        # z
        text_depth = QtWidgets.QLabel("z")
        hbox.addWidget(text_depth)
        text_depth.setFixedWidth(5)
        self.set_depth = QtWidgets.QSpinBox()
        self.set_depth.setFixedWidth(70)
        self.set_depth.setMaximum(9999)
        self.set_depth.setValue(2)
        hbox.addWidget(self.set_depth)
        hbox.addSpacing(20)
        # t
        self.text_time = QtWidgets.QLabel("t")
        hbox.addWidget(self.text_time)
        self.text_time.setFixedWidth(5)
        self.text_time.setVisible(False)
        self.set_time = QtWidgets.QSpinBox()
        self.set_time.setFixedWidth(70)
        self.set_time.setMaximum(9999)
        self.set_time.setValue(3)
        self.set_time.setVisible(False)
        hbox.addStretch()

        hbox.addWidget(self.set_time)
        hbox.addStretch()
        self.set_time.valueChanged.connect(self._toggle_time_tag)

        # z type
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_z_type = QtWidgets.QLabel("Z type:")
        hbox.addWidget(text_z_type)
        text_z_type.setFixedWidth(lbl_width)
        self.set_z_type = QtWidgets.QComboBox()
        self.set_z_type.setFixedWidth(wdg_width)
        self.set_z_type.addItems(['depth', 'elevation'])
        hbox.addWidget(self.set_z_type)
        hbox.addStretch()

        # Comma is decimal seperator
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        decimal_seperator = QtWidgets.QLabel("Decimal Separator:")
        hbox.addWidget(decimal_seperator)
        decimal_seperator.setFixedWidth(lbl_width)
        self.decimal_separator = QtWidgets.QComboBox()
        self.decimal_separator.setFixedWidth(wdg_width)
        self.decimal_separator.addItems(['.', ','])
        hbox.addWidget(self.decimal_separator)
        hbox.addStretch()

        # depth shift
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_shift = QtWidgets.QLabel("Apply shift:")
        hbox.addWidget(text_shift)
        text_shift.setFixedWidth(lbl_width)
        self.set_shift = QtWidgets.QDoubleSpinBox()
        self.set_shift.setFixedWidth(wdg_width)
        self.set_shift.setMinimum(-9999.0)
        self.set_shift.setMaximum(9999.0)
        self.set_shift.setDecimals(3)
        self.set_shift.setValue(0.0)
        hbox.addWidget(self.set_shift)
        hbox.addStretch()

        # FAU endianess
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_endianess = QtWidgets.QLabel("FAU endianess:")
        hbox.addWidget(text_endianess)
        text_endianess.setFixedWidth(lbl_width)
        self.set_endianess = QtWidgets.QComboBox()
        self.set_endianess.setFixedWidth(wdg_width)
        self.set_endianess.addItems(['little', 'big'])
        hbox.addWidget(self.set_endianess)
        hbox.addStretch()

        self.vbox.addSpacing(12)

        self.vbox.addStretch()

        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        hbox.addStretch()
        export_button = QtWidgets.QPushButton('Convert')
        hbox.addWidget(export_button)
        export_button.clicked.connect(self.on_convert)
        hbox.addStretch()

    def on_set_input_path(self) -> None:
        logger.debug('setting input file/folder ...')

        if self._folder_processing:
            # noinspection PyTypeChecker
            selection = QtWidgets.QFileDialog.getExistingDirectory(self, "Set input folder",
                                                                   QtCore.QSettings().value("xyz2fau_input_folder"))
        else:
            # noinspection PyTypeChecker
            selection, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Set input text file",
                                                                 QtCore.QSettings().value("xyz2fau_input_folder"),
                                                                 "Text Files (*.asc *.txt *.xyz *.pts)")
        if selection == "":
            logger.debug('setting input: aborted')
            return
        self.input_path.clear()
        self.input_path.addItem(selection)
        QtCore.QSettings().setValue("xyz2fau_input_folder", os.path.dirname(selection))
        logger.debug("selected path: %s" % selection)

        self.output_path.clear()
        if self._folder_processing:
            self.output_path.addItem("%s" % selection)
        else:
            self.output_path.addItem("%s.fau" % os.path.splitext(selection)[0])

    def on_set_output_path(self) -> None:
        logger.debug('setting output file ...')

        if self._folder_processing:
            # noinspection PyTypeChecker
            selection = QtWidgets.QFileDialog.getExistingDirectory(self, "Set output text folder",
                                                                   QtCore.QSettings().value("xyz2fau_input_folder"))
        else:
            # noinspection PyTypeChecker
            selection, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Set output text file",
                                                                 QtCore.QSettings().value("xyz2fau_output_folder"),
                                                                 "FAU Files (*.fau)")
        if selection == "":
            logger.debug('setting output folder: aborted')
            return
        self.output_path.clear()
        self.output_path.addItem(selection)
        QtCore.QSettings().setValue("xyz2fau_output_folder", os.path.dirname(selection))
        logger.debug("selected path: %s" % selection)

    def on_convert(self) -> None:
        logger.debug('converting xyz file(s) ...')

        if self.input_path.count() == 0:
            QtWidgets.QMessageBox.warning(self, "Input issue", "First set the input path",
                                          QtWidgets.QMessageBox.StandardButton.Ok)
            return

        if self.output_path.count() == 0:
            QtWidgets.QMessageBox.warning(self, "Output issue", "First set the output path",
                                          QtWidgets.QMessageBox.StandardButton.Ok)
            return

        if self._folder_processing:
            self._on_convert_folder()
        else:
            self._on_convert_file()

    def _on_convert_file(self) -> None:
        to_convert = [(self.input_path.item(0).text(), self.output_path.item(0).text())]
        success: bool = self._convert(to_convert)
        if success:
            QtWidgets.QMessageBox.information(self, "Output created", "File converted!",
                                              QtWidgets.QMessageBox.StandardButton.Ok)
        else:
            QtWidgets.QMessageBox.information(self, "Output not created", "No file was made!",

                                              QtWidgets.QMessageBox.StandardButton.Ok)

    def _on_convert_folder(self) -> None:
        to_convert = []
        input_folder = self.input_path.item(0).text()
        output_folder = self.output_path.item(0).text()
        files_skipped = 0
        for base, folders, files in os.walk(os.path.abspath(input_folder)):
            for file in files:
                if os.path.splitext(file)[1].lower() not in (".xyz", ".asc", ".txt", ".pts"):
                    continue

                input_path = os.path.join(base, file)
                output_path = os.path.join('%s.fau' % os.path.splitext(input_path)[0])
                logger.debug("Storing converted file as {}".format(output_path))

                if output_folder is not None:
                    output_path = output_path.replace(os.path.abspath(input_folder),
                                                      os.path.abspath(output_folder))
                    os.makedirs(os.path.dirname(output_path), exist_ok=True)

                if not self.set_overwrite.isChecked() and os.path.exists(output_path):
                    logger.warning("Fau file already exists. Skipping: {}".format(output_path))
                    files_skipped += 1
                    continue
                to_convert.append((str(input_path), str(output_path)))
            if not self.set_recursive.isChecked():
                break
        self._convert(to_convert)
        success: bool = self._convert(to_convert)
        if success:
            QtWidgets.QMessageBox.information(self, "Output created",
                                              "{} files converted!\n{} files skipped!".format(len(to_convert),
                                                                                              files_skipped),
                                              QtWidgets.QMessageBox.StandardButton.Ok)
        else:
            QtWidgets.QMessageBox.information(self, "Output not created", "Not all files were made!",
                                              QtWidgets.QMessageBox.StandardButton.Ok)

    def _toggle_crs_selection(self, state: bool) -> None:
        self.input_crs_combo.setVisible(state)
        self.text_input_crs.setVisible(state)

        self.output_crs_combo.setVisible(state)
        self.text_output_crs.setVisible(state)

        if not state:
            self.input_epsg_code = None
            self.output_epsg_code = None

    def _toggle_ndv(self, state: bool) -> None:
        self.set_ndv.setVisible(state)

        if not state:
            self.user_set_ndv = None

    def _toggle_time_tag(self, state: bool) -> None:
        self.set_time.setVisible(state)
        self.text_time.setVisible(state)
        if state:
            self.timetag_index = self.set_time.value()
        if not state:
            self.timetag_index = None

    def _convert(self, to_convert: list[tuple[str, str]]) -> bool:
        depth_shift = self.set_shift.value()
        if depth_shift == 0.0:
            depth_shift = None

        if not self.set_ndv_cb.isChecked():
            user_set_ndv = None
        else:
            user_set_ndv = self.set_ndv.value()

        split_value = self.set_separator.currentText()
        if split_value == 'TAB':
            split_value = '\t'
        elif split_value == 'SPACE':
            split_value = None

        if self.set_geographic.isChecked():
            self.input_epsg_code = self.input_crs_dict[self.input_crs_combo.currentText()]
            self.output_epsg_code = self.output_crs_dict[self.output_crs_combo.currentText()]

        prj = Project()
        comma_is_decimal_separator: bool = self.decimal_separator.currentText() == ","
        # We cant split on ',' and use comma_is_decimal_separator, that introduce ambiguity
        if split_value == ',' and comma_is_decimal_separator:
            QtWidgets.QMessageBox.warning(self, "Input Error", 'Comma cannot both separate columns and decimals',
                                          QtWidgets.QMessageBox.StandardButton.Ok)
            return False
        all_converted: bool = True
        for input_file, output_file in to_convert:
            print(self.set_ndv.value())
            ret = prj.convert_from_xyz(input_path=input_file,
                                       output_path=output_file,
                                       input_epsg_code=self.input_epsg_code,
                                       output_epsg_code=self.output_epsg_code,
                                       skip_rows=self.set_skip.value(),
                                       little_endian=(self.set_endianess.currentText() == "little"),
                                       depth_shift=depth_shift,
                                       easting_idx=self.set_easting.value(),
                                       northing_idx=self.set_northing.value(),
                                       depth_idx=self.set_depth.value(),
                                       time_idx=self.timetag_index,
                                       split_value=split_value,
                                       z_is_elevation=(self.set_z_type.currentText() == "elevation"),
                                       comma_is_decimal_separator=comma_is_decimal_separator,
                                       user_set_ndv=user_set_ndv
                                       )
            logger.debug('generated: %s' % ret)
            if not ret:
                all_converted = False
        return all_converted

    def _populate_input_crs(self) -> None:
        self.input_crs_dict["EPSG:4326 WGS 84"] = 4326
        self.input_crs_dict["EPSG:4258 ETRS89"] = 4258
        self.input_crs_dict["EPSG:4747 GR96"] = 4747

    def _populate_output_crs(self) -> None:
        etrs_start = 25829
        gr_start = 3179
        wgs_start = 32619

        for idx in range(1, 5):
            self.output_crs_dict["EPSG:%d ETRS89 / UTM zone %dN" % (etrs_start + idx, 29 + idx)] = etrs_start + idx

        for idx in range(1, 12):
            self.output_crs_dict["EPSG:%d GR96 / UTM zone %dN" % (gr_start + idx, 19 + idx)] = gr_start + idx

        for idx in range(1, 15):
            self.output_crs_dict["EPSG:%d WGS 84 / UTM zone %dN" % (wgs_start + idx, 19 + idx)] = wgs_start + idx
