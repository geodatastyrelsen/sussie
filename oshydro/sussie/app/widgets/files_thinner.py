import logging
import os
from typing import TYPE_CHECKING

from PySide6 import QtWidgets, QtGui

if TYPE_CHECKING:
    from oshydro.sussie.survey.project import Project

logger = logging.getLogger(__name__)


class FilesThinner(QtWidgets.QMainWindow):

    def __init__(self, files: list[str], prj: 'Project',
                 parent: QtWidgets.QMainWindow | None = None, title: str | None = "Files Thinner") -> None:
        QtWidgets.QMainWindow.__init__(self, parent)

        if isinstance(title, str):
            self.setWindowTitle(title)

        for f in files:
            if not os.path.exists(f) or not os.path.isfile(f):
                raise RuntimeError('Unable to locate %s' % f)

        self._files = files
        self._prj = prj

        self.setMinimumSize(400, 200)
        self.resize(600, 300)

        frame = QtWidgets.QFrame()
        self.setCentralWidget(frame)

        self.vbox = QtWidgets.QVBoxLayout()
        frame.setLayout(self.vbox)

        lbl_width = 100

        # input files
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_files = QtWidgets.QLabel("Input Files:")
        hbox.addWidget(text_files)
        text_files.setMinimumWidth(lbl_width)
        self.input_files = QtWidgets.QListWidget()
        hbox.addWidget(self.input_files)
        self.input_files.setAlternatingRowColors(True)
        self.input_files.addItems(self._files)

        # output filename
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_output = QtWidgets.QLabel("Output File:")
        hbox.addWidget(text_output)
        text_output.setMinimumWidth(lbl_width)
        self.output_file = QtWidgets.QListWidget()
        self.output_file.setFixedHeight(24)
        hbox.addWidget(self.output_file)
        self.output_file.setAlternatingRowColors(True)
        output_button = QtWidgets.QPushButton("..")
        output_button.setFixedWidth(30)
        hbox.addWidget(output_button)
        output_button.clicked.connect(self.on_set_output_file)

        self.vbox.addSpacing(6)

        # output format
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_res = QtWidgets.QLabel("Grid Resolution [m]:")
        hbox.addWidget(text_res)
        # text_add_grids.setFixedHeight(GuiSettings.single_line_height())
        text_res.setMinimumWidth(lbl_width)
        self.set_res = QtWidgets.QLineEdit()
        self.set_res.setValidator(QtGui.QDoubleValidator(0.00001, 9999, 5, self.set_res))
        self.set_res.setText("10.0")
        self.set_res.setFixedWidth(50)
        hbox.addWidget(self.set_res)
        hbox.addStretch()

        # skip flagged points
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_flagged = QtWidgets.QLabel("Skip flagged points:")
        hbox.addWidget(text_flagged)
        text_flagged.setMinimumWidth(lbl_width)
        self.skip_flagged = QtWidgets.QCheckBox()
        self.skip_flagged.setChecked(True)
        hbox.addWidget(self.skip_flagged)
        hbox.addStretch()

        self.vbox.addStretch()

        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        hbox.addStretch()
        export_button = QtWidgets.QPushButton('Thin')
        hbox.addWidget(export_button)
        export_button.clicked.connect(self.on_thin)
        hbox.addStretch()

    def on_set_output_file(self) -> None:
        logger.debug('setting output file')

        output_path, _ = QtWidgets.QFileDialog.getSaveFileName(self,
                                                               "Set output filename", self._prj.output_folder,
                                                               "XYZ Format (*.xyz);;Shapefile Format (*.shp)")
        if output_path is None:
            logger.debug('Aborted by user')
            return

        logger.debug('output: %s' % output_path)
        self.output_file.clear()
        self.output_file.addItem(output_path)

    def on_thin(self) -> None:
        logger.debug('thinning files ...')

        if self.output_file.count() != 1:
            msg = "You need to first set the output filename"
            QtWidgets.QMessageBox.critical(self, "Thinning", msg, QtWidgets.QMessageBox.StandardButton.Ok)
            return

        self._prj.thin_files(input_paths=self._files, output_path=self.output_file.item(0).text(),
                             grid_size=float(self.set_res.text()), skip_flagged=self.skip_flagged.isChecked())

        nr_files = len(self._files)
        if nr_files == 1:
            msg_txt = "File thinned."
        else:
            msg_txt = "Thinned %d files." % nr_files
        QtWidgets.QMessageBox.information(self, "Output created", msg_txt, QtWidgets.QMessageBox.StandardButton.Ok)
