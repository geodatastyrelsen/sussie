import logging
import os

from PySide6 import QtWidgets

from oshydro.sussie.survey.project import Project

logger = logging.getLogger(__name__)


class FilesExporter(QtWidgets.QMainWindow):

    def __init__(self, files: list[str], prj: 'Project',
                 parent: QtWidgets.QMainWindow | None = None, title: str | None = "Files Exporter") -> None:
        QtWidgets.QMainWindow.__init__(self, parent)

        if isinstance(title, str):
            self.setWindowTitle(title)

        for f in files:
            if not os.path.exists(f) or not os.path.isfile(f):
                raise RuntimeError('Unable to locate %s' % f)

        self._files = files
        self._prj = prj

        self.setMinimumSize(400, 200)
        self.resize(600, 300)

        frame = QtWidgets.QFrame()
        self.setCentralWidget(frame)

        self.vbox = QtWidgets.QVBoxLayout()
        frame.setLayout(self.vbox)

        lbl_width = 100

        # input files
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_files = QtWidgets.QLabel("Input Files:")
        hbox.addWidget(text_files)
        text_files.setMinimumWidth(lbl_width)
        self.input_files = QtWidgets.QListWidget()
        hbox.addWidget(self.input_files)
        self.input_files.setAlternatingRowColors(True)
        self.input_files.addItems(self._files)

        self.vbox.addSpacing(6)

        # output format
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_formats = QtWidgets.QLabel("Output Format:")
        hbox.addWidget(text_formats)
        # text_add_grids.setFixedHeight(GuiSettings.single_line_height())
        text_formats.setMinimumWidth(lbl_width)
        self.ext_xyz = QtWidgets.QRadioButton('.xyz')
        self.ext_xyz.setChecked(True)
        self.ext_xyz.toggled.connect(self.on_change_xyz)
        hbox.addWidget(self.ext_xyz)
        self.ext_shp = QtWidgets.QRadioButton('.shp')
        self.ext_shp.toggled.connect(self.on_change_shp)
        hbox.addWidget(self.ext_shp)
        hbox.addStretch()

        # skip flagged points
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_flagged = QtWidgets.QLabel("Skip flagged points:")
        hbox.addWidget(text_flagged)
        text_flagged.setMinimumWidth(lbl_width)
        self.skip_flagged = QtWidgets.QCheckBox()
        self.skip_flagged.setChecked(True)
        hbox.addWidget(self.skip_flagged)
        hbox.addStretch()

        # save in input folder
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_save_in_input = QtWidgets.QLabel("Save in input folder:")
        hbox.addWidget(text_save_in_input)
        text_save_in_input.setMinimumWidth(lbl_width)
        self.save_in_input = QtWidgets.QCheckBox()
        self.save_in_input.setChecked(False)
        hbox.addWidget(self.save_in_input)
        hbox.addStretch()

        self.vbox.addStretch()

        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        hbox.addStretch()
        export_button = QtWidgets.QPushButton('Export')
        hbox.addWidget(export_button)
        export_button.clicked.connect(self.on_export)
        hbox.addStretch()

    def on_export(self) -> None:
        logger.debug('exporting files ...')

        unique_basenames = set()
        for fn in self._files:
            basename = os.path.basename(fn)
            if basename not in unique_basenames:
                unique_basenames.add(basename)
            else:
                msg = "Duplicated basename: %s.\nPlease select only 1 filename for each basename!" % basename
                QtWidgets.QMessageBox.critical(self, "Export", msg, QtWidgets.QMessageBox.StandardButton.Ok)
                return

        if self.ext_xyz.isChecked():
            output_format = Project.OutputFormat.XYZ
        elif self.ext_shp.isChecked():
            output_format = Project.OutputFormat.SHP
        else:
            raise RuntimeError('Unsupported status for format')

        self._prj.export_files(input_paths=self._files, output_format=output_format,
                               skip_flagged=self.skip_flagged.isChecked(),
                               save_in_input_folder=self.save_in_input.isChecked())

        nr_files = len(self._files)
        if self._prj.progress.canceled:
            msg_txt = "File export CANCELLED!."
        elif nr_files == 1:
            msg_txt = "File exported."
        else:
            msg_txt = "Exported %d files." % nr_files
        QtWidgets.QMessageBox.information(self, "Output created", msg_txt, QtWidgets.QMessageBox.StandardButton.Ok)

    def on_change_xyz(self) -> None:
        if self.ext_xyz.isChecked():
            logger.debug('selected output: .xyz')

    def on_change_shp(self) -> None:
        if self.ext_shp.isChecked():
            logger.debug('selected output: .shp')
