import logging
from typing import TYPE_CHECKING

from PySide6 import QtWidgets
from oshydro.sussie.app.widgets.datagram_viewer import DatagramViewer
from oshydro.sussie.common.qtcustom.custom_qspinbox import CustomQSpinBox

if TYPE_CHECKING:
    from oshydro.base.formats.raw.raw_body import RawBody
    from oshydro.base.formats.raw.raw_header import RawHeader

logger = logging.getLogger(__name__)


class PingViewer(QtWidgets.QMainWindow):

    def __init__(self, header: 'RawHeader | None', body: 'RawBody', parent: QtWidgets.QMainWindow | None = None,
                 force_show_by_datagrams: bool = False, title: str | None = None) -> None:
        QtWidgets.QMainWindow.__init__(self, parent)
        if isinstance(title, str):
            self.setWindowTitle(title)

        self._h = header
        self._b = body

        frame = QtWidgets.QFrame()
        self.setCentralWidget(frame)

        self.vbox = QtWidgets.QVBoxLayout()
        frame.setLayout(self.vbox)

        self.settings = QtWidgets.QGroupBox('Settings')
        self.vbox.addWidget(self.settings)

        if self._h is None or force_show_by_datagrams:
            self._ui_dg()

        elif (self._h.nr_of_pings == 0) or (self._h.nr_of_beams == 0):

            self._ui_dg()

        else:

            hbox = QtWidgets.QHBoxLayout()
            self.settings.setLayout(hbox)

            label = QtWidgets.QLabel('Ping [0-%d]' % (self._h.nr_of_pings - 1))
            hbox.addWidget(label)
            self.pinger = CustomQSpinBox()
            self.pinger.setMinimum(0)
            self.pinger.setMaximum(self._h.nr_of_pings - 1)
            self.pinger.valueChanged.connect(self.on_change_ping_beam)
            hbox.addWidget(self.pinger)

            hbox.addStretch()

            label = QtWidgets.QLabel('Beam [0-%d]' % (self._h.nr_of_beams - 1))
            hbox.addWidget(label)
            self.beamer = CustomQSpinBox()
            self.beamer.setMinimum(0)
            self.beamer.setMaximum(self._h.nr_of_beams - 1)
            self.beamer.valueChanged.connect(self.on_change_ping_beam)
            hbox.addWidget(self.beamer)

        self.ping = QtWidgets.QGroupBox('Data')
        self.vbox.addWidget(self.ping)

        self.ping_vbox = QtWidgets.QVBoxLayout()
        self.ping.setLayout(self.ping_vbox)

        dg = self._b.get_datagram(0)
        self.data = DatagramViewer(data_dict=dg, var_dict=self._b.v['body'])
        self.ping_vbox.addWidget(self.data)

    def _ui_dg(self) -> None:
        hbox = QtWidgets.QHBoxLayout()
        self.settings.setLayout(hbox)

        label = QtWidgets.QLabel('Datagram [0-%d]' % (self._b.nr_of_datagrams - 1))
        hbox.addWidget(label)
        spinner = QtWidgets.QSpinBox()
        spinner.setMinimum(0)
        spinner.setMaximum(self._b.nr_of_datagrams - 1)
        spinner.valueChanged.connect(self.on_change_datagram)
        hbox.addWidget(spinner)

    def on_change_datagram(self, idx: int) -> None:
        logger.debug('new datagram: %s' % idx)

        dg = self._b.get_datagram(idx)
        data = DatagramViewer(data_dict=dg, var_dict=self._b.v['body'])
        self.ping_vbox.replaceWidget(self.data, data)
        self.data = data

    def on_change_ping_beam(self) -> None:
        ping = self.pinger.value()
        beam = self.beamer.value()
        logger.debug('new ping: %s, beam: %s' % (ping, beam))

        dg = self._b.get_sounding(ping=ping, beam=beam)
        data = DatagramViewer(data_dict=dg, var_dict=self._b.v['body'])
        self.ping_vbox.replaceWidget(self.data, data)
        self.data = data
