import logging
import os
from typing import TYPE_CHECKING

from PySide6 import QtWidgets, QtCore

from oshydro.sussie.common.progress.qt_progress import QtProgress
from oshydro.sussie.survey.project import Project

if TYPE_CHECKING:
    from oshydro.base.formats.fau.raw_header import RawHeader
logger = logging.getLogger(__name__)


class FileModifier(QtWidgets.QMainWindow):

    def __init__(self, input_path: str | None = None, parent: QtWidgets.QMainWindow | None = None,
                 title: str | None = None) -> None:
        QtWidgets.QMainWindow.__init__(self, parent)
        if isinstance(title, str):
            self.setWindowTitle(title)

        frame = QtWidgets.QFrame()
        self.setCentralWidget(frame)

        self.vbox = QtWidgets.QVBoxLayout()
        frame.setLayout(self.vbox)

        lbl_width = 100
        wdg_width = 80

        # input filename
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_files = QtWidgets.QLabel("Input File:")
        hbox.addWidget(text_files)
        text_files.setFixedWidth(lbl_width)
        self.input_file = QtWidgets.QListWidget()
        self.input_file.setFixedHeight(32)
        hbox.addWidget(self.input_file)
        self.input_file.setAlternatingRowColors(True)
        if input_path:
            self.input_file.addItem(input_path)
        input_button = QtWidgets.QPushButton("..")
        input_button.setFixedWidth(30)
        hbox.addWidget(input_button)
        input_button.clicked.connect(self.on_set_input_file)

        # output filename
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_output = QtWidgets.QLabel("Output File:")
        hbox.addWidget(text_output)
        text_output.setFixedWidth(lbl_width)
        self.output_file = QtWidgets.QListWidget()
        self.output_file.setFixedHeight(32)
        hbox.addWidget(self.output_file)
        self.output_file.setAlternatingRowColors(True)
        if input_path:
            self.output_file.addItem("%s_mod%s" % os.path.splitext(input_path))
        output_button = QtWidgets.QPushButton("..")
        output_button.setFixedWidth(30)
        hbox.addWidget(output_button)
        output_button.clicked.connect(self.on_set_output_file)

        self.vbox.addSpacing(12)

        # Transducer depth
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        self.apply_rot_bbox = QtWidgets.QCheckBox()
        hbox.addWidget(self.apply_rot_bbox)
        text_rot_bbox = QtWidgets.QLabel("Validate Rotated BBox fields:")
        hbox.addWidget(text_rot_bbox)
        text_rot_bbox.setFixedWidth(lbl_width * 2)
        self.set_rot_bbox = QtWidgets.QSpinBox()
        self.set_rot_bbox.setFixedWidth(wdg_width)
        self.set_rot_bbox.setMinimum(0)
        self.set_rot_bbox.setMaximum(7)
        self.set_rot_bbox.setValue(1)
        hbox.addWidget(self.set_rot_bbox)
        hbox.addStretch()

        # Transducer depth
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        self.apply_tr_depth = QtWidgets.QCheckBox()
        hbox.addWidget(self.apply_tr_depth)
        text_tr_depth = QtWidgets.QLabel("Set Transducer Depth (cm):")
        hbox.addWidget(text_tr_depth)
        text_tr_depth.setFixedWidth(lbl_width * 2)
        self.set_tr_depth = QtWidgets.QSpinBox()
        self.set_tr_depth.setFixedWidth(wdg_width)
        self.set_tr_depth.setMinimum(-9999)
        self.set_tr_depth.setMaximum(9999)
        self.set_tr_depth.setValue(0)
        hbox.addWidget(self.set_tr_depth)
        hbox.addStretch()

        self.vbox.addSpacing(12)

        self.vbox.addStretch()

        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        hbox.addStretch()
        export_button = QtWidgets.QPushButton('Modify')
        hbox.addWidget(export_button)
        export_button.clicked.connect(self.on_modify)
        hbox.addStretch()

    def on_set_input_file(self) -> None:
        logger.debug('setting input file ...')

        selection, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Set input text file",
                                                             str(QtCore.QSettings().value("fau2_input_folder")),
                                                             "FAU Files (*.fau)")
        if selection == "":
            logger.debug('setting input folder: aborted')
            return
        self.input_file.clear()
        self.input_file.addItem(selection)
        QtCore.QSettings().setValue("fau2_input_folder", os.path.dirname(selection))
        logger.debug("selected path: %s" % selection)
        if self.output_file.count() == 0:
            self.output_file.addItem("%s.fau" % os.path.splitext(selection)[0])

    def on_set_output_file(self) -> None:
        logger.debug('setting output file ...')

        selection, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Set output text file",
                                                             str(QtCore.QSettings().value("fau2_output_folder")),
                                                             "FAU Files (*.fau)")
        if selection == "":
            logger.debug('setting output folder: aborted')
            return
        self.output_file.clear()
        self.output_file.addItem(selection)
        QtCore.QSettings().setValue("fau2_output_folder", os.path.dirname(selection))
        logger.debug("selected path: %s" % selection)

    def on_modify(self) -> None:
        logger.debug('converting xyz file ...')

        if self.input_file.count() == 0:
            QtWidgets.QMessageBox.warning(self, "Input issue", "First set the input file",
                                          QtWidgets.QMessageBox.StandardButton.Ok)
            return

        if self.output_file.count() == 0:
            QtWidgets.QMessageBox.warning(self, "Output issue", "First set the output file",
                                          QtWidgets.QMessageBox.StandardButton.Ok)
            return

        has_changes = False
        rot_bbox = None
        if self.apply_rot_bbox.isChecked():
            rot_bbox = self.set_rot_bbox.value()
            has_changes = True
        tr_depth = None
        if self.apply_tr_depth.isChecked():
            tr_depth = self.set_tr_depth.value()
            has_changes = True

        if not has_changes:
            QtWidgets.QMessageBox.warning(self, "Flagged fields", "First flag at least 1 field\nto be modified.",
                                          QtWidgets.QMessageBox.StandardButton.Ok)
            return

        # Actually creating the modified clone

        def hdr_modify(hdr: 'RawHeader') -> None:
            if rot_bbox:
                hdr.d['fields']['rot_rect_valid'] = rot_bbox

                hdr.d['fields']['bb_tilt_x'] = hdr.d['fields']['bb_min_e']
                hdr.d['fields']['bb_tilt_y'] = hdr.d['fields']['bb_min_n']
                hdr.d['fields']['bb_tilt_w'] = hdr.d['fields']['bb_max_e'] - hdr.d['fields']['bb_min_e']
                hdr.d['fields']['bb_tilt_h'] = hdr.d['fields']['bb_max_n'] - hdr.d['fields']['bb_min_n']

            if tr_depth:
                hdr.d['fields']['transducer_depth'] = tr_depth

        # def dg_modify(dg: dict, idx: int):
        #     # dgf = dg['fields']
        #     # logger.debug('%d: %s' % (idx, dgf))
        #     return

        prj = Project(progress=QtProgress(self.parent()))
        ret = prj.make_modified_clone(input_path=self.input_file.item(0).text(),
                                      output_path=self.output_file.item(0).text(),
                                      hdr_modifier=hdr_modify, dg_modifier=None)
        logger.debug('modified: %s' % ret)

        QtWidgets.QMessageBox.information(self, "Output created", "File modified!",
                                          QtWidgets.QMessageBox.StandardButton.Ok)
