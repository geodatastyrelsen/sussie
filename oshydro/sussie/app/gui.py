import logging
import sys
import traceback

from PySide6 import QtCore, QtWidgets

from oshydro.sussie.app.app_style import AppStyle
from oshydro.sussie.app.mainwin import MainWin

logger = logging.getLogger(__name__)


def qt_custom_handler(msg_type: QtCore.QtMsgType, error_context: QtCore.QMessageLogContext, message: str) -> None:
    if "Cannot read property 'id' of null" in message:
        return
    if "GLImplementation:" in message:
        return
    if "Release of profile requested but WebEnginePage" in message:
        return
    if msg_type in (QtCore.QtMsgType.QtWarningMsg,
                    QtCore.QtMsgType.QtCriticalMsg,
                    QtCore.QtMsgType.QtFatalMsg):
        logger.error("Qt error: %s [%s] -> %s" % (msg_type, error_context, message))
        for line in traceback.format_stack():
            logger.debug("- %s" % line.strip())


QtCore.qInstallMessageHandler(qt_custom_handler)


def gui() -> None:
    """Run the gui"""

    sys.argv.append("--disable-web-security")  # temporary fix for CORS warning (QTBUG-70228)
    app = QtWidgets.QApplication(sys.argv)
    app.setStyleSheet(AppStyle.load_stylesheet())

    main_win = MainWin()
    main_win.show()
    # main.do()

    sys.exit(app.exec())
