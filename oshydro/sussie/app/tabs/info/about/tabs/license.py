import logging

from PySide6 import QtWidgets
from oshydro.sussie import license_path

logger = logging.getLogger(__name__)


class LicenseTab(QtWidgets.QWidget):

    def __init__(self, parent: QtWidgets.QWidget | None = None) -> None:
        super().__init__(parent)

        self.layout = QtWidgets.QVBoxLayout()
        self.setLayout(self.layout)

        self.text = QtWidgets.QTextBrowser()
        self.text.setObjectName("LicenseTextBrowser")
        self.text.setReadOnly(True)
        self.text.setLineWrapMode(QtWidgets.QTextBrowser.LineWrapMode.WidgetWidth)
        self.text.setMinimumWidth(200)
        with open(license_path, "r") as fid:
            self.text.setText(fid.read())
        self.layout.addWidget(self.text)
