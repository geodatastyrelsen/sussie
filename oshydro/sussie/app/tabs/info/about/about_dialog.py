import logging

from PySide6 import QtCore, QtGui, QtWidgets

from oshydro.sussie import name, __version__, favicon_path
from oshydro.sussie.app.tabs.info.about.tabs.general_info import GeneralInfoTab
from oshydro.sussie.app.tabs.info.about.tabs.license import LicenseTab
from oshydro.sussie.app.tabs.info.about.tabs.local_environment import LocalEnvironmentTab

logger = logging.getLogger(__name__)


class AboutDialog(QtWidgets.QDialog):

    def __init__(self, parent: QtWidgets.QWidget = None) -> None:
        super().__init__(parent)

        self.setObjectName("AboutDialog")
        self.setWindowTitle("About")
        self.setMinimumSize(400, 200)
        self.resize(620, 350)

        top_layout = QtWidgets.QHBoxLayout()
        self.setLayout(top_layout)

        # left layout

        left_widget = QtWidgets.QWidget()
        left_widget.setMaximumWidth(180)
        top_layout.addWidget(left_widget)
        left_layout = QtWidgets.QVBoxLayout()
        left_widget.setLayout(left_layout)
        left_layout.addStretch()
        logo_layout = QtWidgets.QHBoxLayout()
        left_layout.addLayout(logo_layout)
        logo_layout.addStretch()
        self.logo = QtWidgets.QLabel()
        self.logo.setPixmap(QtGui.QPixmap(favicon_path).scaled(90, 90, QtCore.Qt.AspectRatioMode.KeepAspectRatio))
        self.logo.resize(90, 90)
        # self.logo.setScaledContents(False)
        logo_layout.addWidget(self.logo)
        logo_layout.addStretch()
        self.name = QtWidgets.QLabel("%s v.%s" % (name, __version__))
        self.name.setObjectName("AboutName")
        self.name.resize(100, 100)
        self.name.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.name.setWordWrap(True)
        left_layout.addWidget(self.name)
        left_layout.addStretch()

        # right layout

        right_layout = QtWidgets.QVBoxLayout()
        top_layout.addLayout(right_layout)
        self.tab_widget = QtWidgets.QTabWidget(self)
        self.tab_widget.setTabPosition(QtWidgets.QTabWidget.TabPosition.South)
        self.general_info_tab = GeneralInfoTab(parent=self)
        self.tab_widget.addTab(self.general_info_tab, "Overview")
        self.license_tab = LicenseTab(parent=self)
        self.tab_widget.addTab(self.license_tab, "License")
        self.local_environment_tab = LocalEnvironmentTab(self)
        self.tab_widget.addTab(self.local_environment_tab, "Local")
        self.tab_widget.setCurrentIndex(0)
        right_layout.addWidget(self.tab_widget)

    def switch_visible(self) -> None:
        if self.isVisible():
            self.hide()
        else:
            self.show()
