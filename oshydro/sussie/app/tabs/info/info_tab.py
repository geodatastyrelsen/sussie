import logging
import os

from PySide6 import QtCore, QtGui, QtWidgets
from oshydro.sussie import name, author_email, __author__, favicon_path
from oshydro.sussie.app.tabs.info.about.about_dialog import AboutDialog
from oshydro.sussie.common.browser.browser import Browser
from oshydro.sussie.common.helper import Helper

logger = logging.getLogger(__name__)


class InfoTab(QtWidgets.QMainWindow):
    media = os.path.join(os.path.dirname(__file__), "media")

    def __init__(self, main_win: QtWidgets.QMainWindow,
                 tab_name: str = "App Info Tab", start_url: str | None = None,
                 default_url: str = "https://www.oshydro.org",
                 with_online_manual: bool = False,
                 with_offline_manual: bool = False,
                 with_bug_report: bool = False,
                 with_oshydro_link: bool = False
                 ) -> None:
        super().__init__(main_win)
        self.default_url = default_url
        self.settings = QtCore.QSettings()

        self.setWindowTitle(tab_name)
        self.setContentsMargins(0, 0, 0, 0)

        # add main frame and layout
        self.frame = QtWidgets.QFrame(parent=self)
        self.setCentralWidget(self.frame)
        self.frame_layout = QtWidgets.QVBoxLayout()
        self.frame.setLayout(self.frame_layout)

        if start_url is None:
            start_url = Helper.web_url()
        self.start_url = start_url

        # add browser
        self.browser = Browser(url=self.start_url)
        self.frame_layout.addWidget(self.browser)

        # add about dialog
        self.about_dlg = AboutDialog(parent=self)
        self.about_dlg.hide()

        icon_size = QtCore.QSize(24, 24)

        self.toolbar = self.addToolBar('Shortcuts')
        self.toolbar.setIconSize(icon_size)

        # home
        self.home_action = QtGui.QAction(QtGui.QIcon(os.path.join(self.media, 'home.png')), 'Home page', self)
        self.home_action.triggered.connect(self.load_default)
        self.toolbar.addAction(self.home_action)

        # online manual
        self.open_online_manual_action = None
        if with_online_manual:
            self.open_online_manual_action = QtGui.QAction(
                QtGui.QIcon(os.path.join(self.media, 'online_manual.png')), 'Online Manual', self)
            self.open_online_manual_action.setStatusTip('Open the online manual')
            self.open_online_manual_action.triggered.connect(self.open_online_manual)
            self.toolbar.addAction(self.open_online_manual_action)

        # offline manual
        self.open_offline_manual_action = None
        if with_offline_manual:
            self.open_offline_manual_action = QtGui.QAction(
                QtGui.QIcon(os.path.join(self.media, 'offline_manual.png')), 'Offline Manual', self)
            self.open_offline_manual_action.setStatusTip('Open the offline manual')
            self.open_offline_manual_action.triggered.connect(self.open_offline_manual)
            self.toolbar.addAction(self.open_offline_manual_action)

        # bug report
        self.fill_bug_report_action = None
        if with_bug_report:
            self.fill_bug_report_action = QtGui.QAction(
                QtGui.QIcon(os.path.join(self.media, 'bug.png')), 'Bug Report', self)
            self.fill_bug_report_action.setStatusTip('Fill a bug report')
            self.fill_bug_report_action.triggered.connect(self.fill_bug_report)
            self.toolbar.addAction(self.fill_bug_report_action)

        self.toolbar.addSeparator()

        # oshydro.org
        self.hyo_action = None
        if with_oshydro_link:
            self.hyo_action = QtGui.QAction(
                QtGui.QIcon(os.path.join(self.media, 'oshydro.png')), 'oshydro.org', self)
            self.hyo_action.triggered.connect(self.load_oshydro_org)
            self.toolbar.addAction(self.hyo_action)

        self.toolbar.addSeparator()

        # authors
        self.authors_dialog = None
        self.authors_action = QtGui.QAction(QtGui.QIcon(os.path.join(self.media, 'authors.png')), 'Contacts', self)
        self.authors_action.setStatusTip('Contact Authors')
        self.authors_action.triggered.connect(self.show_authors)
        self.toolbar.addAction(self.authors_action)

        # about
        self.show_about_action = QtGui.QAction(QtGui.QIcon(favicon_path), 'About', self)
        self.show_about_action.setStatusTip('Info about %s' % name)
        self.show_about_action.triggered.connect(self.about_dlg.switch_visible)
        self.toolbar.addAction(self.show_about_action)

    # ### ICON RESIZE ###

    def set_toolbars_icon_size(self, icon_size: int) -> None:
        self.toolbar.setIconSize(QtCore.QSize(icon_size, icon_size))

    # ### ACTIONS ###

    def load_default(self) -> None:
        self.browser.change_url(self.default_url)

    @staticmethod
    def open_online_manual() -> None:
        logger.debug("open online manual")
        Helper.explore_folder("https://www.oshydro.org/manuals/sussie/index.html")

    def open_offline_manual(self) -> None:
        logger.debug("open offline manual")
        pdf_path = os.path.join(self.media, "Sussie.pdf")
        if not os.path.exists(pdf_path):
            logger.warning("unable to find offline manual at %s" % pdf_path)
            return

        Helper.explore_folder(pdf_path)

    @classmethod
    def fill_bug_report(cls) -> None:
        logger.debug("fill bug report")
        raise RuntimeError("USER")

    def load_oshydro_org(self) -> None:
        url = 'https://www.oshydro.org'
        self.browser.change_url(url)

    def show_authors(self) -> None:

        if self.authors_dialog is None:
            # create an author dialog
            self.authors_dialog = QtWidgets.QDialog(self)
            self.authors_dialog.setWindowTitle("Write us")
            self.authors_dialog.setMaximumSize(QtCore.QSize(150, 120))
            self.authors_dialog.setMaximumSize(QtCore.QSize(300, 240))
            vbox = QtWidgets.QVBoxLayout()
            self.authors_dialog.setLayout(vbox)

            hbox = QtWidgets.QHBoxLayout()
            vbox.addLayout(hbox)
            hbox.addStretch()
            logo = QtWidgets.QLabel()
            logo.setPixmap(QtGui.QPixmap(favicon_path))
            hbox.addWidget(logo)
            hbox.addStretch()

            vbox.addSpacing(10)

            text0 = QtWidgets.QLabel(self.authors_dialog)
            text0.setOpenExternalLinks(True)
            vbox.addWidget(text0)
            txt = """
            <b>For bugs and troubleshooting:</b><br>
            <a href=\"mailto:%s?Subject=%s\">%s</a>
            <br><br>""" % (author_email, name, author_email)

            txt += "<b>For comments and ideas for new features:</b><br>\n"
            author_names = __author__.split(";")
            author_emails = author_email.split(";")
            for idx, _ in enumerate(author_names):
                txt += "%s  <a href=\"mailto:%s?Subject=%s\">%s</a><br>\n" \
                       % (author_names[idx], author_emails[idx], name,
                          author_emails[idx])
            text0.setText(txt)

        self.authors_dialog.show()

    def change_url(self, url: str) -> None:
        self.browser.change_url(url)
