import logging

from oshydro.sussie.app.gui import gui
from oshydro.sussie.common.logging import LoggingSetup

logger = logging.getLogger()

LoggingSetup.set_logging(ns_list=["oshydro.sussie"])

gui()
