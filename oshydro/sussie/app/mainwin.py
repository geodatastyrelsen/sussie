import logging
import os
import socket
import ssl
import sys
import traceback
from urllib.error import URLError
from urllib.request import urlopen

from PySide6 import QtCore, QtGui, QtWidgets

from oshydro.sussie import name, __version__
from oshydro.sussie.app.exception.exception_dialog import ExceptionDialog
from oshydro.sussie.app.tabs.info.info_tab import InfoTab
from oshydro.sussie.app.tabs.sussie.sussie_tab import SussieTab
from oshydro.sussie.common.helper import Helper

logger = logging.getLogger(__name__)


class MainWin(QtWidgets.QMainWindow):
    here = os.path.abspath(os.path.dirname(__file__))
    media = os.path.join(here, "media")

    def __init__(self) -> None:
        QtWidgets.QMainWindow.__init__(self)
        logger.debug("Configuration:\n%s" % Helper.package_info())

        # set the application name
        self.name = name
        self.version = __version__
        self.setWindowTitle('%s v.%s' % (self.name, self.version))
        self.setMinimumSize(500, 500)
        self.resize(920, 840)
        _app = QtCore.QCoreApplication.instance()
        _app.setApplicationName('%s' % self.name)
        _app = QtCore.QCoreApplication.instance()
        _app.setOrganizationName("OSHydro")
        _app.setOrganizationDomain("oshydro.org")

        # set icons
        icon_info = QtCore.QFileInfo(os.path.join(self.media, 'favicon.png'))
        self.setWindowIcon(QtGui.QIcon(icon_info.absoluteFilePath()))
        if Helper.is_windows():

            try:
                # This is needed to display the app icon on the taskbar on Windows 7
                import ctypes
                app_id = '%s v.%s' % (self.name, self.version)
                ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(app_id)

            except AttributeError as e:
                logger.debug("Unable to change app icon: %s" % e)

        # make tabs
        self.tabs = QtWidgets.QTabWidget()
        self.setCentralWidget(self.tabs)
        self.tabs.setIconSize(QtCore.QSize(52, 52))
        # - sussie
        self.tab_sussie = SussieTab(main_win=self)
        idx = self.tabs.insertTab(0, self.tab_sussie,
                                  QtGui.QIcon(os.path.join(self.media, 'survey.png')), "")
        self.tabs.setTabToolTip(idx, "Support Utility for Survey Submission Integration Evaluation")
        # - info
        self.tab_info = InfoTab(main_win=self, with_online_manual=True, with_offline_manual=True,
                                with_bug_report=True, with_oshydro_link=True)
        idx = self.tabs.insertTab(9, self.tab_info, QtGui.QIcon(os.path.join(self.media, 'info.png')), "")
        self.tabs.setTabToolTip(idx, "Help")

        # init default settings
        settings = QtCore.QSettings()
        start_tab = settings.value("start_tab")
        # noinspection PyTypeChecker
        if (start_tab is None) or (start_tab > 1):
            start_tab = 0
            settings.setValue("start_tab", start_tab)
        self.tabs.setCurrentIndex(start_tab)

        self.statusBar().setStyleSheet("QStatusBar{color:rgba(0,0,0,128);font-size: 8pt;}")
        self.status_bar_normal_style = self.statusBar().styleSheet()
        self.statusBar().showMessage("%s" % __version__, 2000)

        self.update_gui()
        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.update_gui)
        timer.start(300000)  # 5 mins

        sys.excepthook = self.exception_hook  # install the exception hook

    def update_gui(self) -> None:
        msg = str()
        tokens = list()

        new_release = False
        new_bugfix = False
        latest_version = None
        try:
            # TODO: fix this hack: https://clay-atlas.com/us/blog/2021/09/26/python-en-urllib-error-ssl-certificate/
            # noinspection PyUnresolvedReferences,PyProtectedMember
            ssl._create_default_https_context = ssl._create_unverified_context
            response = urlopen('https://www.oshydro.org/projects/sussie/latest.txt', timeout=1)
            latest_version = response.read().split()[0].decode()

            cur_maj, cur_min, cur_fix = __version__.split('.')
            lat_maj, lat_min, lat_fix = latest_version.split('.')

            if int(lat_maj) > int(cur_maj):
                new_release = True

            elif (int(lat_maj) == int(cur_maj)) and (int(lat_min) > int(cur_min)):
                new_release = True

            elif (int(lat_maj) == int(cur_maj)) and (int(lat_min) == int(cur_min)) and (int(lat_fix) > int(cur_fix)):
                new_bugfix = True

        except (URLError, ssl.SSLError, socket.timeout) as e:
            logger.info("unable to check latest release: %s" % e)

        except ValueError as e:
            logger.info("unable to parse version: %s" % e)

        if new_release:
            logger.info("new release available: %s" % latest_version)
            tokens.append("New release available: %s" % latest_version)
            self.statusBar().setStyleSheet("QStatusBar{background-color:rgba(255,0,0,128);font-size: 8pt;}")

        elif new_bugfix:
            logger.info("new bugfix available: %s" % latest_version)
            tokens.append("New bugfix available: %s" % latest_version)
            self.statusBar().setStyleSheet("QStatusBar{background-color:rgba(255,255,0,128);font-size: 8pt;}")

        else:
            self.statusBar().setStyleSheet(self.status_bar_normal_style)
            logger.debug("latest stable release: %s" % latest_version)

        msg += "|".join(tokens)

        self.statusBar().showMessage(msg, 3000000)

    def change_info_url(self, url: str) -> None:
        self.tab_info.change_url(url)

    def exception_hook(self, ex_type: type[BaseException], ex_value: BaseException, tb: traceback) -> None:
        sys.__excepthook__(ex_type, ex_value, tb)

        # first manage case of not being an exception (e.g., keyboard interrupts)
        if not issubclass(ex_type, Exception):
            msg = str(ex_value)
            if not msg:
                msg = ex_value.__class__.__name__
            logger.info(msg)
            self.close()
            return

        dlg = ExceptionDialog(ex_type=ex_type, ex_value=ex_value, tb=tb)
        ret = dlg.exec()
        if ret == QtWidgets.QDialog.DialogCode.Rejected:
            logger.warning("ignored exception")
            if not dlg.user_triggered:
                self.close()

    # Quitting #

    def do_you_really_want(self, title: str = "Quit", text: str = "quit") -> int:
        msg_box = QtWidgets.QMessageBox(self)
        msg_box.setWindowTitle(title)
        msg_box.setIconPixmap(QtGui.QPixmap(os.path.join(self.media, 'favicon.png')).scaled(QtCore.QSize(60, 60)))
        msg_box.setText('Do you really want to %s?' % text)
        msg_box.setStandardButtons(QtWidgets.QMessageBox.StandardButton.Yes | QtWidgets.QMessageBox.StandardButton.No)
        msg_box.setDefaultButton(QtWidgets.QMessageBox.StandardButton.No)
        return msg_box.exec_()

    def closeEvent(self, event: QtGui.QCloseEvent) -> None:
        """ actions to be done before close the app """
        reply = self.do_you_really_want("Quit", "quit %s" % self.name)

        # reply = QtWidgets.QMessageBox.Yes
        if reply == QtWidgets.QMessageBox.StandardButton.Yes:

            # store current tab
            settings = QtCore.QSettings()
            settings.setValue("start_tab", self.tabs.currentIndex())

            event.accept()
            super().closeEvent(event)

        else:

            event.ignore()
