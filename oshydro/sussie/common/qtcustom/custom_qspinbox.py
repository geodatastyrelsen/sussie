from typing import TYPE_CHECKING

from PySide6 import QtWidgets, QtCore

if TYPE_CHECKING:
    from PySide6.QtGui import QKeyEvent


class CustomQSpinBox(QtWidgets.QSpinBox):
    """Subclass of QtWidgets.QSpinBox which catches custom keyPressEvents"""

    def __init__(self) -> None:
        super().__init__()

    def keyPressEvent(self, event: 'QKeyEvent') -> None:
        if event.key() == QtCore.Qt.Key.Key_Home:
            self.lineEdit().setText(str(self.minimum()))
        elif event.key() == QtCore.Qt.Key.Key_End:
            self.lineEdit().setText(str(self.maximum()))
        else:
            super().keyPressEvent(event)
