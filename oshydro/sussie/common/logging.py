import logging
import os
import shutil


class LoggingSetup:
    _ns_list = None
    _lib_logging = None
    _file_handler = None
    _temp_file_handler = None
    _log_path = None
    _pre_log_path = None
    _temp_log_path = None
    _logfmt = "%(levelname)-9s %(name)s.%(funcName)s:%(lineno)d > %(message)s"

    @classmethod
    def reset_logging(cls) -> None:
        root = logging.getLogger()
        cls.remove_temp_file_logging()
        cls.remove_file_logging()
        for h in root.handlers:
            root.removeHandler(h)
            h.close()
        for f in root.filters:
            root.removeFilter(f)
        _ns_list = None
        _lib_logging = None
        _file_handler = None
        _temp_file_handler = None
        _log_path = None
        _pre_log_path = None
        _temp_log_path = None
        _logfmt = "%(levelname)-9s %(name)s.%(funcName)s:%(lineno)d > %(message)s"

    @classmethod
    def set_logging(cls, ns_list: list[str] | None = None,
                    default_logging: int = logging.WARNING, lib_logging: int = logging.DEBUG,
                    log_path: str = None, include_timestamp: bool = True) -> None:
        """Used to set the logging machinery at the beginning of a script or app

        :param ns_list: List of namespace to log
        :param default_logging: Default level of logging
        :param lib_logging: Logging level for the selected namespaces
        :param log_path: If passed, a log file will be generated
        :param include_timestamp: If true, prepends a %H:%M:%S timestamp to log lines
        """

        cls.reset_logging()

        cls._logfmt = "%(levelname)-9s %(name)s.%(funcName)s:%(lineno)d > %(message)s"
        if include_timestamp:
            cls._logfmt = "%(asctime)s " + cls._logfmt
        handlers = [logging.StreamHandler()]

        if log_path:
            cls._log_path = log_path
            cls._file_handler = logging.FileHandler(cls._log_path)
            handlers.append(cls._file_handler)

        logging.basicConfig(
            level=default_logging,
            format=cls._logfmt,
            datefmt='%H:%M:%S',
            handlers=handlers
        )

        main_ns = "__main__"
        cls._ns_list = ns_list
        if cls._ns_list is None:
            cls._ns_list = [main_ns, ]
        if main_ns not in cls._ns_list:
            cls._ns_list.append(main_ns)

        cls._lib_logging = lib_logging
        for ns in cls._ns_list:
            logging.getLogger(ns).setLevel(cls._lib_logging)

    @classmethod
    def add_temp_file_logging(cls, new_log_path: str) -> None:
        """Temporarily add a log file prepending the past log messages

        :param new_log_path: Path to the file where temporarily logging
        """

        if cls._temp_file_handler is not None:
            raise RuntimeError('first remove current temp file logging')

        if not os.path.exists(cls._log_path):
            raise RuntimeError('unable to locate log file: %s' % cls._log_path)

        if cls._pre_log_path is None:
            init_log_path = '%s.pre' % cls._log_path
            shutil.copy(cls._log_path, init_log_path)
            cls._pre_log_path = init_log_path

        shutil.copy(cls._pre_log_path, new_log_path)

        fh = logging.FileHandler(new_log_path)
        fh.setLevel(logging.DEBUG)
        fh.setFormatter(logging.Formatter(cls._logfmt))
        logging.getLogger().addHandler(fh)

        for ns in cls._ns_list:
            logging.getLogger(ns).setLevel(cls._lib_logging)

        cls._temp_file_handler = fh
        cls._temp_log_path = new_log_path

    @classmethod
    def remove_temp_file_logging(cls) -> None:
        """ Remove the temporary logging to a file if previously added.
        """

        if cls._temp_file_handler is None:
            return

        logging.getLogger().removeHandler(cls._temp_file_handler)
        for ns in cls._ns_list:
            logging.getLogger(ns).removeHandler(cls._temp_file_handler)
        cls._temp_file_handler.close()
        cls._temp_file_handler = None

    @classmethod
    def remove_file_logging(cls) -> None:
        """ Remove the logging to a file if previously added.
        """

        if cls._file_handler is None:
            return

        logging.getLogger().removeHandler(cls._file_handler)
        for ns in cls._ns_list:
            logging.getLogger(ns).removeHandler(cls._file_handler)
        cls._file_handler.close()
        cls._file_handler = None
        cls.file_path = None

    @classmethod
    def remove_pre_log_file(cls) -> None:
        """ If exists, remove the prepended log file
        """
        if os.path.exists(cls._pre_log_path):
            os.remove(cls._pre_log_path)
            cls._pre_log_path = None

    @classmethod
    def remove_log_file(cls) -> None:
        """ If exists, remove the log file
        """
        if os.path.exists(cls._log_path):
            os.remove(cls._log_path)
            cls._log_path = None

    @classmethod
    def remove_temp_log_file(cls) -> None:
        """ If exists, remove the temporary log file
        """
        if os.path.exists(cls._temp_log_path):
            os.remove(cls._temp_log_path)
            cls._temp_log_path = None
