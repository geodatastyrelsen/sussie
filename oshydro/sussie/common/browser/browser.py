import logging
import os

from PySide6 import QtCore, QtGui, QtWidgets, QtWebEngineCore, QtWebEngineWidgets

from oshydro.sussie.common.browser.download_widget import DownloadWidget
from oshydro.sussie.common.helper import Helper

logger = logging.getLogger(__name__)


class WebEnginePage(QtWebEngineCore.QWebEnginePage):

    def javaScriptConsoleMessage(self, level, message, line_number, source_id) -> None:
        if ("slideshare" not in source_id) and ("oshydro.org" not in source_id):
            logger.debug("QWebEngine: %s[#%d] -> %s" % (source_id, line_number, message))


class Browser(QtWidgets.QMainWindow):

    def __init__(self, parent: QtWidgets.QWidget | None = None, url: str = "https://www.oshydro.org") -> None:
        super().__init__(parent)

        self.setWindowTitle('Browser')

        self._actions = {}
        self._create_menu()

        self._tool_bar = QtWidgets.QToolBar()
        self._tool_bar.setIconSize(QtCore.QSize(16, 16))
        self.addToolBar(self._tool_bar)
        for action in self._actions.values():
            if not action.icon().isNull():
                self._tool_bar.addAction(action)

        self.address_line_edit = QtWidgets.QLineEdit()
        self.address_line_edit.setClearButtonEnabled(True)
        self.address_line_edit.returnPressed.connect(self._load)
        self._tool_bar.addWidget(self.address_line_edit)

        self.view = QtWebEngineWidgets.QWebEngineView()
        self.view.settings().setAttribute(QtWebEngineCore.QWebEngineSettings.WebAttribute.JavascriptEnabled,
                                          True)
        self.view.settings().setAttribute(QtWebEngineCore.QWebEngineSettings.WebAttribute.JavascriptCanOpenWindows,
                                          True)
        self.view.settings().setAttribute(
            QtWebEngineCore.QWebEngineSettings.WebAttribute.LocalContentCanAccessRemoteUrls, True)
        self.view.settings().setAttribute(
            QtWebEngineCore.QWebEngineSettings.WebAttribute.LocalContentCanAccessFileUrls, True)
        # self.interceptor = RequestInterceptor()
        self.profile = QtWebEngineCore.QWebEngineProfile()
        # self.profile.setRequestInterceptor(self.interceptor)
        self.profile.downloadRequested.connect(self._download_requested)
        self.profile.setPersistentCookiesPolicy(
            QtWebEngineCore.QWebEngineProfile.PersistentCookiesPolicy.NoPersistentCookies)
        self.profile.setHttpCacheType(QtWebEngineCore.QWebEngineProfile.HttpCacheType.NoCache)
        self.profile.setPersistentStoragePath(self._web_engine_folder())
        self.page = WebEnginePage(self.profile, self.view)
        self.view.setPage(self.page)

        self.view.page().titleChanged.connect(self.setWindowTitle)
        self.view.page().urlChanged.connect(self._url_changed)
        self.setCentralWidget(self.view)

        self.change_url(url=url)

    @classmethod
    def _web_engine_folder(cls) -> str:
        dir_path = os.path.abspath(os.path.join(Helper.oshydro_folder(), "WebEngine"))
        if not os.path.exists(dir_path):  # create it if it does not exist
            os.makedirs(dir_path)
        return dir_path

    def _create_menu(self) -> None:
        style_icons = ':/qt-project.org/styles/commonstyle/images/'

        # noinspection PyArgumentList
        back_action = QtGui.QAction(QtGui.QIcon.fromTheme("go-previous", QtGui.QIcon(style_icons + 'left-32.png')),
                                    "Back", self, shortcut=QtGui.QKeySequence(QtGui.QKeySequence.StandardKey.Back),
                                    triggered=self.back)

        self._actions[QtWebEngineCore.QWebEnginePage.WebAction.Back] = back_action

        # noinspection PyArgumentList
        forward_action = QtGui.QAction(QtGui.QIcon.fromTheme("go-next", QtGui.QIcon(style_icons + 'right-32.png')),
                                       "Forward", self,
                                       shortcut=QtGui.QKeySequence(QtGui.QKeySequence.StandardKey.Forward),
                                       triggered=self.forward)
        self._actions[QtWebEngineCore.QWebEnginePage.WebAction.Forward] = forward_action

        # noinspection PyArgumentList
        reload_action = QtGui.QAction(QtGui.QIcon(style_icons + 'refresh-32.png'), "Reload", self,
                                      shortcut=QtGui.QKeySequence(QtGui.QKeySequence.StandardKey.Refresh),
                                      triggered=self.reload)
        self._actions[QtWebEngineCore.QWebEnginePage.WebAction.Reload] = reload_action

    def change_url(self, url: str) -> None:
        self.address_line_edit.setText(url)
        self._load()

    def url(self) -> str:
        return self.address_line_edit.text()

    def _load(self) -> None:
        url = QtCore.QUrl.fromUserInput(self.address_line_edit.text().strip())
        if url.isValid():
            self.view.load(url)

    def back(self) -> None:
        self.view.page().triggerAction(QtWebEngineCore.QWebEnginePage.WebAction.Back)

    def forward(self) -> None:
        self.view.page().triggerAction(QtWebEngineCore.QWebEnginePage.WebAction.Forward)

    def reload(self) -> None:
        self.view.page().triggerAction(QtWebEngineCore.QWebEnginePage.WebAction.Reload)

    def _url_changed(self, url: QtCore.QUrl) -> None:
        self.address_line_edit.setText(url.toString())

    def _download_requested(self, item: QtWebEngineCore.QWebEngineDownloadRequest) -> None:

        # Remove old downloads before opening a new one
        for old_download in self.statusBar().children():
            if type(old_download).__name__ == 'download_widget' and \
                    old_download.state() != QtWebEngineCore.QWebEngineDownloadRequest.DownloadState.DownloadInProgress:
                self.statusBar().removeWidget(old_download)
                del old_download

        item.accept()
        download_widget = DownloadWidget(item)
        download_widget.remove_requested.connect(self._remove_download_requested,
                                                 QtCore.Qt.ConnectionType.QueuedConnection)

        self.statusBar().addWidget(download_widget)

    def _remove_download_requested(self) -> None:
        download_widget = self.sender()
        self.statusBar().removeWidget(download_widget)
        del download_widget
