import ctypes
import importlib
import logging
import os
import platform
import subprocess
import sys
from datetime import datetime, timedelta, timezone
from pathlib import Path

from oshydro.sussie import name, __version__, __author__, author_email, __license__

logger = logging.getLogger(__name__)


class Helper:
    """ A collection class with many helper functions, in alphabetic order """

    epoch = datetime(1970, 1, 1, tzinfo=timezone.utc)
    dep_dict = {
        "matplotlib": "matplotlib",
        "oshydro.base": "oshydro.base",
        "PySide6": "PySide6"
    }

    @classmethod
    def check_supported_python(cls, supported_versions: list[str]) -> None:
        running_python_version = '%d.%d' % (sys.version_info.major, sys.version_info.minor)
        if running_python_version not in supported_versions:
            logger.warning("Unsupported Python version: '%s'. This tool supports: %s."
                           % (running_python_version, supported_versions))

    @classmethod
    def explore_folder(cls, path: Path | str) -> bool:
        """Open the passed path using OS-native commands"""
        if isinstance(path, Path):
            path = str(path)

        if cls.is_url(path):
            import webbrowser
            webbrowser.open(path)
            return True

        if not os.path.exists(path):
            logger.warning('invalid path to folder: %s' % path)
            return False

        path = os.path.normpath(path)

        if cls.is_darwin():
            subprocess.call(['open', '--', path])
            return True

        elif cls.is_linux():
            subprocess.call(['xdg-open', path])
            return True

        elif cls.is_windows():
            subprocess.call(['explorer', path])
            return True

        logger.warning("Unknown/unsupported OS")
        return False

    def explore_package_folder(self) -> None:
        self.explore_folder(self.package_folder())

    @classmethod
    def files(cls, folder: str, ext: str, subfolder: str | None = None) -> list[str]:
        file_list = list()
        for root, _, files in os.walk(folder):

            # logger.info("root: %s, folder: %s" % (root, subfolder))
            if subfolder is not None:
                if subfolder not in root:
                    continue

            for f in files:
                ext_list = ext.split(',')
                for ext_item in ext_list:
                    if f.endswith(ext_item):
                        file_list.append(os.path.join(root, f))
        return file_list

    @classmethod
    def file_size(cls, file_path: str) -> int:
        """file size in bytes and timestamp as datetime"""
        if not os.path.exists(file_path):
            raise RuntimeError("the passed file does not exist: %s" % file_path)

        return os.stat(file_path).st_size

    @classmethod
    def file_size_timestamp(cls, file_path: str) -> tuple[int, datetime]:
        """file size in bytes and timestamp as datetime"""
        if not os.path.exists(file_path):
            raise RuntimeError("the passed file does not exist: %s" % file_path)

        file_stat = os.stat(file_path)
        mod_timestamp = cls.epoch + timedelta(seconds=file_stat.st_mtime)
        file_sz = file_stat.st_size
        return file_sz, mod_timestamp

    @classmethod
    def is_64bit_os(cls) -> bool:
        """ Check if the current OS is at 64 bits """
        return platform.machine().endswith('64')

    @classmethod
    def is_64bit_python(cls) -> bool:
        """ Check if the current Python is at 64 bits """
        return platform.architecture()[0] == "64bit"

    @classmethod
    def is_darwin(cls) -> bool:
        """ Check if the current OS is macOS """
        return sys.platform == 'darwin'

    @classmethod
    def is_linux(cls) -> bool:
        """ Check if the current OS is Linux """
        return sys.platform in ['linux', 'linux2']

    @classmethod
    def is_pydro(cls) -> bool:
        try:
            import HSTB as _
            return True

        except ImportError:
            return False

    @classmethod
    def is_url(cls, value: str) -> bool:
        if len(value) > 7:

            https = "http"
            if value[:len(https)] == https:
                return True

        return False

    @classmethod
    def is_user_admin(cls) -> bool:
        if cls.is_windows():

            if ctypes.windll.shell32.IsUserAnAdmin():
                logger.debug("user is admin")
                return True
            else:
                logger.debug("user is not admin")
                return False

        else:
            raise RuntimeError("Windows only")

    @classmethod
    def is_windows(cls) -> bool:
        """ Check if the current OS is Windows """
        return (sys.platform == 'win32') or (os.name == "nt")

    @classmethod
    def python_path(cls) -> str:
        """ Return the python site-specific directory prefix (the temporary folder for PyInstaller) """

        # required by PyInstaller
        if hasattr(sys, '_MEIPASS'):
            if cls.is_windows():
                # noinspection PyPackageRequirements
                import win32api
                # noinspection PyProtectedMember
                return win32api.GetLongPathName(sys._MEIPASS)
            else:
                # noinspection PyProtectedMember
                return sys._MEIPASS

        # check if in a virtual environment
        if hasattr(sys, 'real_prefix'):

            if cls.is_windows():
                # noinspection PyPackageRequirements
                import win32api
                return win32api.GetLongPathName(sys.real_prefix)
            else:
                return sys.real_prefix

        return sys.prefix

    @classmethod
    def timestamp(cls) -> str:
        return datetime.now().strftime("%Y%m%d_%H%M%S")

    @classmethod
    def truncate_too_long(cls, path: str, max_path_length: int = 260, left_truncation: bool = False) -> str:

        max_len = max_path_length

        path_len = len(path)
        if path_len < 260:
            return path

        logger.debug("path truncation required since path would be longer than %d [left: %s]"
                     % (max_len, left_truncation))

        folder_path, filename = os.path.split(path)
        file_base, file_ext = os.path.splitext(filename)

        if left_truncation:
            new_len = max_len - len(folder_path) - len(file_ext) - 2
            if new_len < 1:
                raise RuntimeError("the passed path is too long: %d" % path_len)
            path = os.path.join(folder_path, file_base[(len(file_base) - new_len):] + file_ext)

        else:
            new_len = max_len - len(folder_path) - len(file_ext) - 2
            if new_len < 1:
                raise RuntimeError("the passed path is too long: %d" % path_len)
            path = os.path.join(folder_path, file_base[:new_len] + file_ext)

        return path

    @classmethod
    def package_info(cls, qt_html: bool = False) -> str:

        def style_row(raw_row: str, is_h1: bool = False, is_h2: bool = False) -> str:
            if qt_html:
                if is_h1:
                    return "<h1>%s</h1>" % raw_row
                elif is_h2:
                    return "<h2>%s</h2>" % raw_row
                else:
                    return "<p>%s</p>" % raw_row
            else:
                if is_h1:
                    return "%s:\n" % raw_row
                elif is_h2:
                    return "[%s]\n" % raw_row
                else:
                    return "  - %s\n" % raw_row

        def style_mailto(text: str, url: str) -> str:
            if qt_html:
                return "<a href=\"mailto:%s?Subject=%s%%20v.%s\" target=\"_top\">%s</a>" \
                    % (url, name, __version__, text,)
            else:
                return "%s(%s)" % (text, url)

        def package_version(package: str) -> str:
            try:
                return importlib.import_module("%s" % package).__version__
            except (ImportError, AttributeError):
                try:
                    return importlib.import_module("%s" % package).VERSION
                except (ImportError, AttributeError):
                    return "N/A"

        msg = str()

        msg += style_row("General Info", is_h2=True)
        msg += style_row("version: %s" % __version__)
        msg += style_row("author: %s" % style_mailto(__author__, author_email))
        msg += style_row("license: %s" % __license__)

        msg += style_row("Hosting Environment", is_h2=True)
        msg += style_row("os: %s %s-bit" % (os.name, "64" if cls.is_64bit_os() else "32"))
        msg += style_row("python: %s %s-bit" % (platform.python_version(), "64" if cls.is_64bit_python() else "32"))
        msg += style_row("pydro: %s" % cls.is_pydro())

        msg += style_row("Dependencies", is_h2=True)
        for key in cls.dep_dict.keys():
            msg += style_row("%s: %s" % (key, package_version(cls.dep_dict[key]),))

        return msg

    @classmethod
    def package_folder(cls) -> str:
        _dir = os.path.join(os.getenv("localappdata"), "OSHydro", name)
        if not os.path.exists(_dir):  # create it if it does not exist
            os.makedirs(_dir)

        return _dir

    @classmethod
    def oshydro_folder(cls) -> str:
        return os.path.abspath(os.path.join(cls.package_folder(), os.pardir))

    @classmethod
    def web_url(cls, suffix: str | None = None) -> str:

        url = 'https://www.oshydro.org/projects/%s/%s' \
              % (name.lower(), __version__)

        if suffix and isinstance(suffix, str):
            url += "_" + suffix

        return url
