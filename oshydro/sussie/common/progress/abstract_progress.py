import logging
from abc import ABCMeta, abstractmethod

logger = logging.getLogger(__name__)


class AbstractProgress(metaclass=ABCMeta):
    """Abstract class for a progress bar"""

    def __init__(self) -> None:
        self._title = str()
        self._text = str()

        self._min = 0
        self._max = 100
        self._range = self._max - self._min
        self._value = 0

        self._is_disabled = False
        self._is_canceled = False

    @property
    def value(self) -> int:
        return self._value

    @property
    def text(self) -> str:
        return self._text

    @property
    def title(self) -> str:
        return self._title

    @property
    def min(self) -> int:
        return self._min

    @property
    def max(self) -> int:
        return self._max

    @property
    def range(self) -> int:
        self._range = self._max - self._min
        return self._range

    @property
    @abstractmethod
    def canceled(self) -> bool:
        pass

    @abstractmethod
    def start(self, title: str = "Progress", text: str = "Processing", min_value: int = 0, max_value: int = 100,
              init_value: int = 0,
              has_abortion: bool = False, is_disabled: bool = False) -> None:
        pass

    @abstractmethod
    def update(self, value: int | None = None, text: str | None = None, restart: bool = False) -> None:
        pass

    @abstractmethod
    def add(self, quantum: float, text: str | None = None) -> None:
        pass

    @abstractmethod
    def end(self) -> None:
        pass
