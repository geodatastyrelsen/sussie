import logging
import os

from PySide6 import QtWidgets
from oshydro.base.formats.fau.raw import Raw

from oshydro.sussie.app.widgets.ping_viewer import PingViewer
from oshydro.sussie.common.logging import LoggingSetup
from oshydro.sussie.common.testing import Testing

LoggingSetup.set_logging(ns_list=["oshydro.sussie"])
logger = logging.getLogger(__name__)

# This example visualizes a .fau file with header by pings and beams

# Set the path to a .fau file with header

testing = Testing()
input_path = os.path.join(testing.input_data_folder(), 'fau', 'valid_le_with_header.fau')
if not os.path.exists(input_path):
    raise RuntimeError('Unable to locate %s' % input_path)
logger.debug("Input path: %s" % input_path)

# Open the .fau file

raw = Raw(path=input_path)
raw.check_variant()
raw.read_header()
raw.read_body()

# Create a Qt application, then use the PingViewer to visualize the file content

app = QtWidgets.QApplication([])

pv = PingViewer(header=raw.header, body=raw.body, title='test')
pv.show()

exit(app.exec())
