import logging

from PySide6 import QtWidgets

from oshydro.sussie.app.widgets.files_thinner import FilesThinner
from oshydro.sussie.common.logging import LoggingSetup
from oshydro.sussie.common.testing import Testing
from oshydro.sussie.survey.project import Project

LoggingSetup.set_logging(ns_list=["oshydro.sussie"])
logger = logging.getLogger(__name__)

# This example visualizes the header and the first datagram of a .fau file with header

# Set the path to a .fau file with header

testing = Testing()
input_paths = testing.input_test_files(ext='.fau')[:4]
logger.debug("Input paths: %s" % len(input_paths))

prj = Project()
prj.output_folder = testing.output_data_folder()

# Create a Qt application, then use the DatagramViewer to visualize header and first datagram

app = QtWidgets.QApplication([])

fe = FilesThinner(files=input_paths, prj=prj)
fe.show()

exit(app.exec())
