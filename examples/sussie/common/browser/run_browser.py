import logging
import sys

from PySide6 import QtWidgets

from oshydro.sussie.common.browser.browser import Browser
from oshydro.sussie.common.logging import LoggingSetup

logger = logging.getLogger(__name__)
LoggingSetup.set_logging(ns_list=["oshydro.sussie"])

# This example opens a browser with the passed url

sys.argv.append("--disable-web-security")
app = QtWidgets.QApplication(sys.argv)

w = Browser()
w.show()

new_url = "https://www.oshydro.org/projects/sussie/sussie"
w.change_url(new_url)
logger.debug("new url: %s" % w.url())

sys.exit(app.exec())
