import logging

from PySide6 import QtWidgets

from oshydro.sussie.common.logging import LoggingSetup
from oshydro.sussie.common.progress.qt_progress import QtProgress
from oshydro.sussie.common.testing import Testing
from oshydro.sussie.survey.project import Project

LoggingSetup.set_logging(ns_list=["oshydro.sussie"])
logger = logging.getLogger(__name__)

# This example compares a file with itself so that the output of the comparison should be equal.

testing = Testing()
input_paths = testing.input_test_files(ext='.fau', subfolder='fau')
logger.debug("nr. of files: %s" % len(input_paths))

input_1 = input_paths[0]
logger.debug('input 1: %s' % input_1)
input_2 = input_paths[0]
logger.debug('input 2: %s' % input_2)

app = QtWidgets.QApplication([])
wdg = QtWidgets.QWidget()
prj = Project(progress=QtProgress(parent=wdg))

ret = prj.is_binary_equal(first_path=input_1, second_path=input_2)
logger.debug('binary equal: %s' % ret)
logger.debug('differences: %s' % (prj.bin_diff_list,))

ret = prj.is_semantically_equal(first_path=input_1, second_path=input_2)
logger.debug('semantically equal: %s' % ret)
logger.debug('differences: %s' % (prj.sem_diff_list,))
