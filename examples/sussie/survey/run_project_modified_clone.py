import logging
import os
from random import uniform

from oshydro.base.formats.fau.raw_header import RawHeader

from oshydro.sussie.common.logging import LoggingSetup
from oshydro.sussie.common.testing import Testing
from oshydro.sussie.survey.project import Project

LoggingSetup.set_logging(ns_list=["oshydro.sussie"])
logger = logging.getLogger(__name__)

# This example takes a .fu2 file and create a modified clone of it.

# Set a path to a fu2 file and prepare the output path adding 'mod_' to its basename

testing = Testing()
input_paths = testing.input_test_files(ext='.fu2', subfolder='fu2')
logger.debug("nr. of files: %s" % len(input_paths))
input_path = input_paths[0]
logger.debug('input: %s' % input_path)
output_path = os.path.join(testing.output_data_folder(), 'mod_%s' % os.path.basename(input_path))
logger.debug('output: %s' % output_path)


# This function is called for modifying the header. No-op in case of files without header.

def hdr_modify(hdr: RawHeader):
    hdr.d['fields']['sv_name'] = b'test.svp'


# This function is called for modifying each datagram.

def dg_modify(dg: dict, idx: int) -> dict:
    dgf = dg['fields']
    # logger.debug('%d: %s' % (idx, dgf))

    # Special handling for specific datagrams using their indices
    if idx in [0, 10]:
        dgf['n'] = 0
        dgf['e'] = 0
        dgf['depth'] = 8880
        dgf['quality'] = 128
        dgf['sec'] += 30000

    else:
        dgf['n'] -= 10000
        dgf['e'] += 20000
        dgf['depth'] = int(uniform(0.75, 0.99) * dgf['depth'])
        dgf['quality'] = 1
        dgf['sec'] += 30000
    return dg


# Actually creating the modified clone

prj = Project()
ret = prj.make_modified_clone(input_path=input_path, output_path=output_path,
                              hdr_modifier=hdr_modify, dg_modifier=dg_modify,
                              max_dg_idx=224)
logger.debug('modified: %s' % ret)

# This last part can be activated to compare original and modified files

check_binary_equality = False
check_semantical_equality = False

if check_binary_equality:
    ret = prj.is_binary_equal(first_path=input_path, second_path=output_path)
    logger.debug('binary equal: %s' % ret)
    logger.debug('differences: %s' % (prj.bin_diff_list,))

if check_semantical_equality:
    ret = prj.is_semantically_equal(first_path=input_path, second_path=output_path)
    logger.debug('semantically equal: %s' % ret)
    logger.debug('differences: %s' % (prj.sem_diff_list,))
