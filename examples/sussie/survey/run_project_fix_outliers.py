import os
import shutil
from datetime import datetime
from typing import Optional

from PySide6 import QtWidgets

from oshydro.sussie.common.logging import LoggingSetup
from oshydro.sussie.common.progress.qt_progress import QtProgress
from oshydro.sussie.common.testing import Testing
from oshydro.sussie.survey.project import Project

# Project is instantiated
app = QtWidgets.QApplication([])
wdg = QtWidgets.QWidget()
prj = Project(progress=QtProgress(parent=wdg))

# Setting up logging to .log file
report_dir = os.path.join(prj.output_folder, 'Log')
dt = datetime.now().strftime('%Y%m%d_%H%M%S')
log_file = os.path.join(report_dir, f'fix_log_outliers_{dt}.log')
os.makedirs(report_dir, exist_ok=True)
LoggingSetup.set_logging(ns_list=["oshydro.sussie"])

json_path: Optional[str] = None  # Path to the full json retrieved from sussie check.

if not json_path:  # Running default Test if not json_path is provided
    testing = Testing()
    test_fau_folder = os.path.join(testing.input_data_folder(), 'fau')
    output_folder = os.path.abspath(testing.output_data_folder())
    input_paths = testing.input_test_files(ext='.fau', subfolder='fau')

    for f in os.listdir(output_folder):
        os.remove(os.path.join(output_folder, f))

    for filepath in input_paths:
        shutil.copy2(filepath, os.path.join(output_folder, os.path.basename(filepath)))
    test_json = testing.input_test_files(ext='.json', subfolder='json')[0]

    with open(test_json, 'r') as fid:
        new_json = fid.read().replace("TEST", output_folder.replace("\\", "/"))
    json_path = os.path.join(output_folder, os.path.basename(test_json))

    with open(json_path, 'w') as fid:
        fid.write(new_json)

prj.positional_outliers(json_path=json_path, print_overview=True, fix_points=True, protected_paths=["F:", "P:", ])
