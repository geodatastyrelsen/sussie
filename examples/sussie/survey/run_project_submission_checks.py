import logging

from PySide6 import QtWidgets

from oshydro.sussie.common.logging import LoggingSetup
from oshydro.sussie.common.testing import Testing
from oshydro.sussie.survey.project import Project

LoggingSetup.set_logging(ns_list=["oshydro.sussie"])
logger = logging.getLogger()

# This example runs the submission check tool on the passed dataset folder.

testing = Testing()
input_folders = testing.input_data_sub_folders(subfolder='datasets')
logger.debug("nr. of input sub-folders: %s" % len(input_folders))

app = QtWidgets.QApplication([])

prj = Project()

schema_list = Project.schema_list()

prj.schema = schema_list[0]
prj.add_to_submission_list(input_folders[0])
ret = prj.check_submission()
logger.debug("successfully validated: %s" % ret)
logger.debug(prj)

# mappestruktur 2022
prj.schema = schema_list[3]
prj.add_to_submission_list(input_folders[1])
ret = prj.check_submission()
logger.debug("successfully validated: %s" % ret)
logger.debug(prj)
prj.open_output_folder()
