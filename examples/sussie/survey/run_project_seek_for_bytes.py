import logging

from oshydro.base.formats.fau.fau_formats.fauformats import FauFormats

from oshydro.sussie.common.logging import LoggingSetup
from oshydro.sussie.common.testing import Testing
from oshydro.sussie.survey.project import Project

LoggingSetup.set_logging(ns_list=["oshydro.sussie"])
logger = logging.getLogger(__name__)

# This example scans a file for bytes matching the passed values

testing = Testing()
input_paths = testing.input_test_files(ext='.fau', subfolder='fau')
logger.debug("nr. of files: %s" % len(input_paths))

seek_value = 613725231
seek_type = 'i'
endianess = FauFormats.Endianess.LITTLE

prj = Project()
prj.seek_for_bytes(input_paths=input_paths, seek_value=seek_value, seek_type=seek_type,
                   endianess=endianess)
