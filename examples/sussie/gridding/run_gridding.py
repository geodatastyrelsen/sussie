import logging
import os

from oshydro.sussie.common.logging import LoggingSetup
from oshydro.sussie.common.testing import Testing
from oshydro.sussie.survey.gridding.raw_gridder import RawGridder

LoggingSetup.set_logging(ns_list=["oshydro.sussie"])
logger = logging.getLogger(__name__)

# This example shows how to use the Raw class to grid .fau files, then export it as xyz

testing = Testing()
input_paths = testing.download_test_files(ext='.fau', subfolder='fau')
grid_size = 20.0  # m
output_path = os.path.join(testing.output_data_folder(),
                           'test.%s.xyz' % ('%sm' % grid_size).replace('.', '_'))

rg = RawGridder(input_paths=input_paths)
rg.make_grid(grid_res=grid_size, output_path=output_path, skip_flagged=True)
