import logging
import os

from oshydro.sussie.common.logging import LoggingSetup

LoggingSetup.set_logging(ns_list=["oshydro.sussie"])
logger = logging.getLogger(__name__)

cur_dir = os.path.abspath(os.path.dirname(__file__))
html_dir = os.path.join(cur_dir, '_build', 'html')
if not os.path.exists(html_dir):
    raise RuntimeError('Unable to locate the html folder: %s' % html_dir)
logger.debug(html_dir)

try:
    os.rename(os.path.join(html_dir, '_images'), os.path.join(html_dir, 'images'))
    os.rename(os.path.join(html_dir, '_static'), os.path.join(html_dir, 'static'))
    os.rename(os.path.join(html_dir, '_sources'), os.path.join(html_dir, 'sources'))
except Exception as e:
    logger.debug('Folders already renamed? %s' % e)


def replace_string(original_str: str, replacement_str: str, root_dir: str):
    for dir_name, dirs, files in os.walk(root_dir):
        for filename in files:
            if os.path.splitext(filename)[-1] in ['.inv', '.png']:
                continue
            fpath = os.path.join(dir_name, filename)
            # logger.debug(fpath)
            with open(fpath, encoding="utf-8") as f:
                s = f.read()
            s = s.replace(original_str, replacement_str)
            with open(fpath, "w", encoding="utf-8") as f:
                f.write(s)


replace_string('_images', 'images', html_dir)
replace_string('_static', 'static', html_dir)
replace_string('_sources', 'sources', html_dir)

nojekyll_path = os.path.join(html_dir, '.nojekyll')
if not os.path.exists(nojekyll_path):
    file = open(nojekyll_path, 'w+')
    file.close()
