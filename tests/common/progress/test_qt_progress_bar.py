import time
import unittest

from PySide6 import QtWidgets

from oshydro.sussie.common.progress.qt_progress import QtProgress


class TestQtProgress(unittest.TestCase):

    def test_run(self) -> None:
        if not QtWidgets.QApplication.instance():
            import sys
            sys.argv.append("--disable-web-security")
            QtWidgets.QApplication(sys.argv)

        widget = QtWidgets.QWidget()
        widget.show()

        progress = QtProgress(widget)

        progress.start(title='Test Bar', text='Doing stuff', min_value=100, max_value=300, init_value=100)

        time.sleep(.1)

        progress.update(value=150, text='Updating')

        time.sleep(.1)

        progress.add(quantum=50, text='Updating')

        time.sleep(.1)

        print("canceled? %s" % progress.canceled)

        progress.end()

        # noinspection PyArgumentList
        # QtWidgets.QApplication.instance().exec_()


def suite() -> unittest.TestSuite:
    s = unittest.TestSuite()
    s.addTests(unittest.TestLoader().loadTestsFromTestCase(TestQtProgress))
    return s
