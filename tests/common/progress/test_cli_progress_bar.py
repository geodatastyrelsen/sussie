import time
import unittest

from oshydro.sussie.common.progress.cli_progress import CliProgress


class TestCliProgress(unittest.TestCase):

    def setUp(self) -> None:
        self.progress = CliProgress()

    def test_start_minimal(self) -> None:
        try:
            self.progress.start()
        except Exception as e:
            self.fail(e)

    def test_start_custom_title_text(self) -> None:
        try:
            self.progress.start(title='Test Bar', text='Doing stuff')
        except Exception as e:
            self.fail(e)

    def test_start_custom_min_max(self) -> None:
        try:
            self.progress.start(min_value=100, max_value=300, init_value=100)
        except Exception as e:
            self.fail(e)

    def test_start_minimal_update(self) -> None:
        try:
            self.progress.start()
            self.progress.update(50)
        except Exception as e:
            self.fail(e)

    def test_start_minimal_update_raising(self) -> None:
        with self.assertRaises(Exception):
            self.progress.start()
            self.progress.update(1000)

    def test_start_minimal_add(self) -> None:
        try:
            self.progress.start()
            self.progress.add(50)
        except Exception as e:
            self.fail(e)

    def test_start_minimal_add_raising(self) -> None:
        with self.assertRaises(Exception):
            self.progress.start()
            self.progress.add(1000)

    def test_run(self) -> None:
        progress = CliProgress()

        progress.start(title='Test Bar', text='Doing stuff', min_value=100, max_value=300, init_value=100)

        time.sleep(.1)

        progress.update(value=150, text='Updating')

        time.sleep(.1)

        progress.add(quantum=50, text='Updating')

        time.sleep(.1)

        self.assertFalse(progress.canceled)

        progress.end()


def suite() -> unittest.TestSuite:
    s = unittest.TestSuite()
    s.addTests(unittest.TestLoader().loadTestsFromTestCase(TestCliProgress))
    return s
