import os
import unittest

from oshydro.sussie.common.helper import Helper


class TestCommonHelper(unittest.TestCase):

    def test_explore_folder(self) -> None:
        self.assertTrue(Helper.explore_folder(__file__))
        self.assertFalse(Helper.explore_folder(__file__ + ".fake"))
        self.assertTrue(Helper.explore_folder(os.path.dirname(__file__)))
        self.assertFalse(Helper.explore_folder(os.path.dirname(__file__) + "fake"))

    def test_is_64bit_os(self) -> None:
        self.assertIsInstance(Helper.is_64bit_os(), bool)

    def test_is_64bit_python(self) -> None:
        self.assertIsInstance(Helper.is_64bit_python(), bool)

    def test_is_darwin_linux_windows(self) -> None:
        self.assertIsInstance(Helper.is_darwin(), bool)
        self.assertIsInstance(Helper.is_linux(), bool)
        self.assertIsInstance(Helper.is_windows(), bool)

        self.assertTrue(any([Helper.is_linux(), Helper.is_darwin(), Helper.is_windows()]))

    def test_is_pydro(self) -> None:
        self.assertIsInstance(Helper.is_pydro(), bool)

    def test_is_url(self) -> None:
        self.assertTrue(Helper.is_url("https://www.oshydro.org"))
        self.assertTrue(Helper.is_url("http://www.oshydro.org"))
        self.assertFalse(Helper.is_url("ftp://fake/url"))

    def test_python_path(self) -> None:
        self.assertTrue(os.path.exists(Helper.python_path()))

    def test_package_info(self) -> None:
        self.assertIsInstance(Helper.package_info(qt_html=True), str)
        self.assertIsInstance(Helper.package_info(qt_html=False), str)

    def test_package_folder(self) -> None:
        self.assertTrue(os.path.exists(Helper.package_folder()))

    def test_oshydro_folder(self) -> None:
        self.assertTrue(os.path.exists(Helper.oshydro_folder()))


def suite() -> unittest.TestSuite:
    s = unittest.TestSuite()
    s.addTests(unittest.TestLoader().loadTestsFromTestCase(TestCommonHelper))
    return s
