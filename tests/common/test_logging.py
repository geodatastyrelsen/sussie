import logging
import unittest

from oshydro.sussie.common.logging import LoggingSetup


class TestSetLogging(unittest.TestCase):

    def test_init(self) -> None:
        logger = logging.getLogger(__name__)
        LoggingSetup.set_logging(ns_list=[__name__])

        logger.debug("debug")
        logger.info("info")
        logger.warning("warning")
        logger.error("error")


def suite() -> unittest.TestSuite:
    s = unittest.TestSuite()
    s.addTests(unittest.TestLoader().loadTestsFromTestCase(TestSetLogging))
    return s
