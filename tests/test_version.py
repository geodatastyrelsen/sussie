import unittest

from oshydro.sussie import __version__


class TestVersion(unittest.TestCase):

    def test_explore_folder(self) -> None:
        self.assertEqual(len(__version__.split('.')), 3)


def suite() -> unittest.TestSuite:
    s = unittest.TestSuite()
    s.addTests(unittest.TestLoader().loadTestsFromTestCase(TestVersion))
    return s
