import unittest

from PySide6 import QtWidgets

from oshydro.sussie.app.exception.exception_dialog import ExceptionDialog


# import logging
# logging.basicConfig(level=logging.DEBUG)


class TestAppExceptionDialog(unittest.TestCase):

    def test_visibility(self) -> None:

        if not QtWidgets.QApplication.instance():
            QtWidgets.QApplication([])

        d = ExceptionDialog()
        d.show()

        # QtWidgets.QApplication.instance().exec_()

    def test_with_user_exception(self) -> None:

        if not QtWidgets.QApplication.instance():
            import sys
            sys.argv.append("--disable-web-security")
            QtWidgets.QApplication(sys.argv)

        d = ExceptionDialog(ex_value=BaseException("USER"))
        d.show()

        # QtWidgets.QApplication.instance().exec_()


def suite() -> unittest.TestSuite:
    s = unittest.TestSuite()
    s.addTests(unittest.TestLoader().loadTestsFromTestCase(TestAppExceptionDialog))
    return s
