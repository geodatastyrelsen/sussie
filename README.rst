Sussie
======

The Support Utility for Survey Submission Integrity Evaluation (Sussie) tool provides functionalities
to handle hydrographic survey data.

Sussie has two main components:

* Submission Checker: evaluate the folder/file structure of the submitted survey.
* Raw Data Explorer: explore the binary content of raw data files.

The `Sussie project <https://www.oshydro.org/projects/sussie/>`_  is part of the Open Source Hydro (`OSHydro <https://www.oshydro.org>`_) initiative.


How to build Sussie
===================

You can build `Sussie <https://bitbucket.org/geodatastyrelsen/sussie/src/master/>`_ using pyinstaller

``pyinstaller --clean -y freeze/Sussie.spec``


How to build Documentation
==========================

Update the markdown files found in ``<path_to_Sussie>\Sussie\docs`` to reflect changes to the current code, updated version numbers and updating the year on the copyright.

The documentation is then built to a ``.pdf`` file using MikTex.

Using a shell, in ``<path_to_Sussie>\Sussie\docs``, run ``make pdf``

This requires MikTex to be installed [Download]. The first time building, it will need to download all the packages used. This can take some time.

The resulting .pdf found in ``<path_to_Sussie>\Sussie\docs\_build\latex``  is then moved to ``\Sussie\oshydro\sussie\app\tabs\info\media\`` , so that it can be accessed from the offline manual button.
